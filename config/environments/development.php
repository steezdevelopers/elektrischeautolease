<?php
/**
 * Configuration overrides for WP_ENV === 'development'
 */

use Roots\WPConfig\Config;

Config::define('SAVEQUERIES', true);
Config::define('WP_DEBUG', true);
Config::define('WP_DEBUG_DISPLAY', false);

ini_set('display_errors', '1');

// Enable plugin and theme updates and installation from the admin
Config::define('DISALLOW_FILE_MODS', false);

if (env('MOBILITY_SERVICE_USERNAME')) {
	define('MOBILITY_SERVICE_USERNAME', env('MOBILITY_SERVICE_USERNAME'));
}
if (env('MOBILITY_SERVICE_PASSWORD')) {
	define('MOBILITY_SERVICE_PASSWORD', env('MOBILITY_SERVICE_PASSWORD'));
}

Config::define('WP_REDIS_DISABLED', true);

<?php
/**
 * Configuration overrides for WP_ENV === 'staging'
 */

use Roots\WPConfig\Config;

/**
 * You should try to keep staging as close to production as possible. However,
 * should you need to, you can always override production configuration values
 * with `Config::define`.
 *
 * Example: `Config::define('WP_DEBUG', true);`
 * Example: `Config::define('DISALLOW_FILE_MODS', false);`
 */

if (env('WP_ROCKET_KEY')) {
	define('WP_ROCKET_KEY', env('WP_ROCKET_KEY'));
}
// Your email, the one you used for the purchase.
if (env('WP_ROCKET_EMAIL')) {
	define('WP_ROCKET_EMAIL', env('WP_ROCKET_EMAIL'));
}

if (env('MOBILITY_SERVICE_USERNAME')) {
	define('MOBILITY_SERVICE_USERNAME', env('MOBILITY_SERVICE_USERNAME'));
}
if (env('MOBILITY_SERVICE_PASSWORD')) {
	define('MOBILITY_SERVICE_PASSWORD', env('MOBILITY_SERVICE_PASSWORD'));
}

define('WP_CACHE', true);

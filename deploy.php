<?php

/**
 * This file is forked from https://discourse.roots.io/t/heres-deployer-recipes-to-deploy-bedrock/9896/5
 */

namespace Deployer;

require __DIR__ . '/vendor/autoload.php';

require 'recipe/common.php';
require 'vendor/cstaelen/deployer-wp-recipes/recipes/assets.php';
require 'vendor/cstaelen/deployer-wp-recipes/recipes/cleanup.php';
require 'vendor/cstaelen/deployer-wp-recipes/recipes/db.php';
require 'vendor/cstaelen/deployer-wp-recipes/recipes/uploads.php';
require 'vendor/vlucas/phpdotenv/src/Dotenv.php';

/**
 * Server Configuration
 */

// Define servers
inventory('servers.yml');

// Default server
set('default_stage', 'staging');
set('keep_releases', 2);

// Setup some Bedrock specific settings
set('wp-recipes', [
	'theme_name' => 'brandfirm',
	'theme_dir' => 'web/app/themes/',
	'shared_dir' => '{{deploy_path}}/shared',
	'assets_dist' => 'web/app/themes/brandfirm/',
	'local_wp_url' => '',
	'remote_wp_url' => '',
	'clean_after_deploy' => [
		'deploy.php',
		'.gitignore',
		'*.md'
	]
]);

/**
 * Bedrock Configuration
 */

// WordPress configuration
set('repository', 'git@bitbucket.org:steezdevelopers/elektrischeautolease.git');
set('shared_files', ['.env', 'web/.htaccess', 'web/.htpasswd', 'web/robots.txt', 'web/app/advanced-cache.php', 'web/app/object-cache.php', 'web/app/7cf71930890ceb7.html']);
set('shared_dirs', ['web/app/uploads', 'web/app/cache', 'web/app/wp-rocket-config', 'web/assets']);
set('writable_dirs', ['web/app/uploads', 'web/app/cache', 'web/app/wp-rocket-config', 'web/assets']);

task('cloudways:reset-file-permissions', function () {
	run("cd {{release_path}}");
	run("/usr/local/bin/wp --path='{{release_path}}/web/wp' plugin activate steez-cw-api-connector");
	run("/usr/local/bin/wp --path='{{release_path}}/web/wp' steez:reset_file_permissions");
});

/**
 * Deploy task
 */
task('deploy', [
	'deploy:prepare',
	'deploy:lock',
	'deploy:release',
	'deploy:update_code',
	'deploy:shared',
	'deploy:vendors',
	'cloudways:reset-file-permissions',
	'deploy:writable',
	'deploy:symlink',
	'deploy:unlock',
	'cleanup'
])->desc('Deploy your Bedrock project');

task('deploy:write', [
	'deploy:prepare',
	'deploy:lock',
	'deploy:release',
	'deploy:update_code',
	'deploy:shared',
	'deploy:vendors',
	'deploy:writable',
	'deploy:symlink',
	'deploy:unlock',
	'cleanup'
])->desc('Deploy your Bedrock project');

after('deploy', 'success');
after('deploy:write', 'success');

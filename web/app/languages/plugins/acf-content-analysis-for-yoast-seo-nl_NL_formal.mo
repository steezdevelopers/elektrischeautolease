��    
      l      �       �   2   �   B   $  <   g  M   �  "   �  p     K   �     �  A   �  ;  ,  ;   h  I   �  2   �  V   !  "   x  v   �  K        ^  A   v               
      	                        %1$s could not be loaded because of missing files. %1$s requires %2$s %3$s (or higher) to be installed and activated. %1$s requires %2$s %3$s or higher, please update the plugin. %1$s requires %2$s (free or pro) 5.7 or higher to be installed and activated. ACF Content Analysis for Yoast SEO Ensure that Yoast SEO analyzes all Advanced Custom Fields 5.7+ content including Flexible Content and Repeaters. Thomas Kräftner, ViktorFroberg, marol87, pekz0r, angrycreative, Team Yoast http://angrycreative.se https://wordpress.org/plugins/acf-content-analysis-for-yoast-seo/ PO-Revision-Date: 2021-08-26 19:39:35+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/3.0.0-alpha.2
Language: nl
Project-Id-Version: Plugins - ACF Content Analysis for Yoast SEO - Stable (latest release)
 %1$s kan niet worden geladen vanwege ontbrekende bestanden. Voor %1$s moet %2$s %3$s (of hoger) worden geïnstalleerd en geactiveerd. %1$s vereist %2$s %3$s of hoger, update de plugin. Voor %1$s moet %2$s (gratis of pro) 5.7 of hoger worden geïnstalleerd en geactiveerd. ACF Content Analysis for Yoast SEO Zorg ervoor dat Yoast SEO alle Advanced Custom Fields 5.7+ inhoud analyseert, inclusief flexibele inhoud en herhalers. Thomas Kräftner, ViktorFroberg, marol87, pekz0r, angrycreative, Team Yoast http://angrycreative.se https://wordpress.org/plugins/acf-content-analysis-for-yoast-seo/ 
��    &      L  5   |      P  
   Q     \     e     s     �  :   �  `   �  L   *     w  �   ~     '     7  �   C  �   �     i     p     u  	   �     �  j   �     	          #     8     A     J     X  
   k     v          �  S   �  4   �  f        �     �     �  �  �     |
     �
     �
     �
     �
  M   �
  l   "  L   �  	   �  �   �     �     �  �   �  �   o  	                  +     9  f   F     �     �     �     �     �     �           -  	   6     @     R  \   W  <   �  b   �     T     ]     e               
           #         	                           $                   &                 "                                          !                           %           Add Author Add Meta Add Post Type Add Taxonomy Administrator Advanced Post Types Order - At least one menu has changed: All interfaces are set to hide, at least a visible is required. You can change tht from Settings All rules are compared to a query using AND operator. For more details check Author Certain changes has been done to your site and some of Query Rules cannot be displayed anymore. You should review the settings Query Rules area and click Update button. Click to toggle Contributor Create a set of criteria rules which match your query. This will determine what to show on the following sort list and the order will apply on front side. Define custom text for the next link. You can use the <strong>%title</strong> variable to include the post title. Leave blank for default. Editor Meta Meta Relation Next Post Notice: There is Polylang: The Sort Rules couldn't be translated automatically. You need to set/translate this manually for Previous Post Query Rules Query Rules examples Re-Order Re-order Show Advanced Sort List Settings Subscriber Taxonomy Taxonomy Relation Term This plugin can't work without javascript, because it's use drag and drop and AJAX. Use commas instead of %s to separate excluded terms. WPML: The Sort Rules couldn't be translated automatically. You need to set/translate this manually for another invalid similar sort Project-Id-Version: Advanced Post Types Order
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2025-01-20 15:43+0200
Last-Translator: NspCode <contact@nsp-code.com>
Language-Team: NspCode <contact@nsp-code.com>
Language: nb
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-KeywordsList: _;gettext;gettext_noop;_e;__
X-Poedit-Basepath: ..
X-Generator: Poedit 3.5
X-Poedit-SearchPath-0: .
 Legg til forfatter Legg til meta Legg til innholdstype Legg til taksonomi Administrator Avansert rekkefølge for innholdstyper - Minst ett menyvalg har blitt endret: Alle grensesnitt er satt til å være skjult, minst ett må være synlig. Du kan endre dette i Innstillinger All rules are compared to a query using AND operator. For more details check Forfatter Visse endringer har blitt gjort på nettstedet ditt, og noen av spørrereglene kan ikke vises lenger. Du bør gjennomgå innstillingene for spørrereglene og klikke på Oppdater-knappen. Klikk for å veksle Bidragsyter Opprett et sett med kriterieregler som matcher spørringen din. Dette vil bestemme hva som vises i den følgende sorteringslisten og hvordan rekkefølgen blir på fremsiden. Definer egendefinert tekst for neste lenke. Du kan bruke <strong>%title</strong> variabelen for å inkludere tittelen på innlegget. La stå tomt for standard. Redaktør Meta Metarelasjon Neste innlegg Merk: Det er Polylang: Sorteringsreglene kunne ikke oversettes automatisk. Du må sette/oversette dette manuelt for Forrige innlegg Spørreregler Eksempler på spørreregler Omorganiser Omorganiser Vis avansert Innstillinger for sorteringslist Abonnent Taksonomi Taksonomirelasjon Term Dette programtillegget fungerer ikke uten javascript, fordi det bruker dra og slipp og AJAX. Bruk komma i stedet for %s for å skille utelukkede vilkår. WPML: Sorteringsreglene kunne ikke oversettes automatisk. Du må sette/oversette dette manuelt for en annen ugyldig lignende sortering 
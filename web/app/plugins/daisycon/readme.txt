﻿=== Daisycon prijsvergelijkers ===
Contributors: Daisycon
Tags: Affiliate marketing, xml feed, vergelijken, vergelijkingssite, Daisycon
Requires at least: 3.4.2
Tested up to: 6.0.1
Donate link:
Stable tag: 4.0
Author: Daisycon
Author URI: https://www.daisycon.com/nl/
License: Daisycon

Promoot adverteerders van Daisycon eenvoudig en goed met de verschillende professionele prijsvergelijkers voor publishers.

== Description ==

Daisycon heeft verschillende professionele prijsvergelijkers voor haar publishers beschikbaar. Met deze WordPress-plugin plaats je deze vergelijkers eenvoudig op je WordPress-website.

Om de vergelijkers in deze plugin te gebruiken moet zijn aangemeld als publisher bij Daisycon. [Meld je gratis en vrijblijvend aan!](http://www.daisycon.com/nl/publishers/ "Meld je gratis en vrijblijvend aan!")

De plugin bevat de volgende prijsvergelijkers:

- [Alles-in-één-vergelijker](https://www.daisycon.com/nl/vergelijkers/alles-in-een-vergelijker/ "Alles-in-één-vergelijker")
- [Boekhoudsoftware-vergelijker](https://www.daisycon.com/nl/vergelijkers/boekhoudsoftware-vergelijker/ "Boekhoudsoftware-vergelijker")
- [Datingsitevergelijker](https://www.daisycon.com/nl/vergelijkers/datingsitevergelijker/ "Datingsitevergelijker")
- [Energievergelijker](https://www.daisycon.com/nl/vergelijkers/energievergelijker/ "Energievergelijker")
- [Leaseautovergelijker](https://www.daisycon.com/nl/vergelijkers/lease-auto-vergelijker/ "Leaseautovergelijker")
- [Reisverzekeringvergelijker](https://www.daisycon.com/nl/vergelijkers/reisverzekeringvergelijker/ "Reisverzekeringvergelijker")
- [Sim only-vergelijker](https://www.daisycon.com/nl/vergelijkers/sim-only-vergelijker/ "Sim only-vergelijker")
- [Telecomvergelijker](https://www.daisycon.com/nl/vergelijkers/telecomvergelijker/ "Telecomvergelijker")
- [Uitvaartkostentool](https://www.daisycon.com/nl/vergelijkers/uitvaartkostentool/ "Uitvaartkostentool")
- [Vakantietool](https://www.daisycon.com/nl/vergelijkers/vakantietool/ "Vakantietool")
- [Zorgverzekeringvergelijker](https://www.daisycon.com/nl/vergelijkers/zorgverzekeringvergelijker/ "Zorgverzekeringvergelijker")

**Voordelen Daisycon prijsvergelijkers**

Voordelen van de prijsvergelijkers van Daisycon:

- Professionele vergelijkers met het beste aanbod van adverteerders bij Daisycon
- Zeer flexibel - Op iedere WordPress-website te plaatsen en aan te passen aan jouw stijl
- Eenvoudig te installeren - Er is geen technische kennis voor nodig.
- Onderhoudsvrij - Daisycon onderhoudt de tools en voegt nieuwe adverteerders automatisch toe
- Uitgebreide support - Heb je een probleem met installeren, mail ons op Wordpress@daisycon.com.


**Over Daisycon en de vergelijkers**

Daisycon is het grootste Nederlandse affiliatenetwerk. Maar haar vergelijkers ondersteunt Daisycon haar publishers bij het optimaliseren van de omzet. Vele honderden websites gebruiken met veel succes de vergelijkers van Daisycon.

Lees meer over [publisher worden bij Daisycon!](http://www.daisycon.com/nl/publishers/ "publisher worden bij Daisycon!")



== Installation ==

Upload de map uit de zip-file en upload deze naar wp-content/plugins binnen je Wordpress installatie. De plugin verschijnt in het linkermenu.

== Frequently Asked Questions ==

[Bekijk hier de uitgebreide FAQ over de vergelijkers van Daisycon.](https://faq-publisher.daisycon.com/hc/nl/sections/201708162-Daisycon-vergelijkers
"FAQ Daisycon vergelijkers")

== Screenshots ==

1. Energievergelijker

2. Zorgverzekeringvergelijker

3. Datingsitevergelijker

4. Boekhoudsoftwarevergelijker

5. Alles-in-één-vergelijker

6. Wijnvergelijker


== Changelog ==

= 4.7 =
* Marktonderzoek vergelijker beheer en tool toegevoegd
* Vakantievergelijker filters bijgewerkt

= 4.6.5 =
* Autoverzekering verborgen uit overzichten (momenteel geen actieve campagnes)

= 4.6.4 =
* Menu icon bijgewerkt

= 4.6.3 =
* Code optimalisaties doorgevoerd

= 4.6.2 =
* Vakantievergelijker beheer en tool toegevoegd

= 4.6.1 =
* Bug opgelost die voorkwam dat Leaseauto vergelijker zichtbaar werd

= 4.6 =
* Van de volgende tool(s) is de optie om filters te verbergen toegevoegd;
 - Alles-in-één-vergelijker
 - Autoverzekeringvergelijker (+ extra kleur instellingen)
 - Boekhoudsoftwarevergelijker (+ extra kleur instellingen)
 - Energievergelijker
 - Zorgverzekeringvergelijker
* Datingsitevergelijker kan nu ook opgenomen worden met alleen xpartners id ingevuld
* Leaseautovergelijker beheer en tool toegevoegd

= 4.5 =
* Probleem opgelost waarbij profielen niet zichtbaar werden (opnieuw aanmaken vereist)
* Algemene screenshots bijgewerkt
* Boekhoudsoftwarevergelijker beheer en tool vernieuwd
* Wijnvergelijker bijgewerkt met filter opties

= 4.4 =
* Wijn vergelijker toegevoegd
* Getest tot en met WordPress 5.8

= 4.3 =
* Alles-in-één-vergelijker beheer en tool vernieuwd

= 4.2 =
* Default actie tekst kleur bij energie en prefill energie bijgewerkt

= 4.1 =
* Prefill energie tool beheer en tool toegevoegd

= 4.0 =
* Update voor de gehele plugin, nieuwe looks en functionaliteiten
* Aan het introductiescherm zijn het Xpartners id en profiel toegevoegd
* Autoverzekeringvergelijker beheer en tool vernieuwd
* Datingsitevergelijker  beheer en tool vernieuwd
* Energievergelijker beheer en tool vernieuwd
* Zorgverzekeringvergelijker beheer en tool vernieuwd

= 3.2.1 =
* Readme aangepast met nieuwe landingspagina's

= 3.2 =
* Hypotheekvergelijker is uitgefaseerd
* Clean-up

= 3.1.2 =
* Bugfix voor waarde 0 invullen bij Telecomvergelijker, Sim Only-vergelijker en Energievergelijker

= 3.1 =
* Alle vergelijkers geupdate, oa
* Alfabetische volgorde toegepast
* Afbeeldingen gewijzigd
* Compatibiliteit met thema's/plugins verbeterd
* MediaID wordt weergegeven en opgeslagen bij introductie en is niet langer nodig in de shortcodes

= 3.0 =
* Update voor alle vergelijkers. Schoenenvergelijker verwijderd. Boekhoudsoftware- en Hypotheekvergelijker toegevoegd.

= 2.6 =
* Update voor Zorgverzekeringvergelijker

= 2.5 =
* Loterijenvergelijker toegevoegd

= 2.4.1 =
* Xpartners support toegevoegd

= 2.4 =
* Schoenenzoeker vervangen voor Schoenenvergelijker
* Uitvaartkostentool toegevoegd
* Diverse bugfixes en opmaak wijzigingen

= 2.3 =
* Schoenenzoeker menu item toegevoegd

= 2.2 =
* Alle tools zijn nu zowel http als https

= 2.1 =
* Belangrijke update voor de energievergelijker om responsive te worden

= 2.0.1 =
* Database connection fix

= 2.0 =
* Alles-in-één-vergelijker toegevoegd

= 1.9.1 =
* Belangrijke bugfix voor datingsitevergelijker

= 1.9 =
* Reisverzekeringvergelijker toegevoegd
* Datingsitevergelijker toegevoegd
* Tekstuele wijzigingen

= 1.8.1 =
* Title tag in Telecom- en Sim Only-vergelijker gefixed

= 1.8 =
* Sim only-vergelijker update

= 1.7 =
* Telecomvergelijker update
* Dagaanbiedingen-tool toegevoegd

= 1.6.1 =
* Bugfix en paar tekstuele wijzigingen

= 1.6 =
* Vakantievergelijker toegevoegd

= 1.5.1 =
* Bugfix voor waarde 0 invullen bij Telecomvergelijker, Sim Only-vergelijker en Energievergelijker

= 1.5 =
* Autoverzekeringvergelijker toegevoegd

= 1.4.1 =
* Bugfix voor 1.4 met betrekking tot de Energievergelijker

= 1.4 =
* Energievergelijker toegevoegd

= 1.3 =
* Wintersportvergelijker toegevoegd

= 1.2 =
* Zorgverzekeringvergelijker toegevoegd

= 1.1 =
* Sim-only vergelijker toegevoegd

= 1.0 =
* Stabiele release

== Upgrade Notice ==

= 3.2 =
Hypotheekvergelijker is uitgefaseerd. Gelieve de shortcode van de website te halen.

= 3.1.2 =
Boekhoudsoftware-vergelijker bugfix, zodat de kleuren ook goed ingeladen worden.

= 3.0 =
Belangrijke update, graag updaten!

= 2.5 =
Loterijenvergelijker toegevoegd

= 2.4.1 =
Xpartners support toegevoegd

= 2.4 =
Nieuwe tools toegevoegd

= 2.2 =
Alle tools zijn nu zowel http als https

= 2.1 =
Belangrijke update voor de energievergelijker om responsive te worden

= 2.0.1 =
Database fix, update voor nieuwe php versies

= 2.0 =
Alles-in-één-vergelijker toegevoegd

= 1.9.1 =
Belangrijke bugfix voor datingsitevergelijker

= 1.9 =
Datingsitevergelijker en reisverzekeringvergelijker toegevoegd

= 1.8.1 =
Title tag in Telecom- en Sim Only-vergelijker gefixed

= 1.8 =
Sim only-vergelijker update gegeven

= 1.7 =
Dagaanbiedingen-tool toegevoegd en Telecomvergelijker update gegeven

= 1.6 =
Vakantievergelijker toegevoegd

= 1.5.1 =
Update als je waarde 0 in wilt vullen bij de Telecomvergelijker, Sim Only-vergelijker en Energievergelijker

= 1.5 =
Autoverzekeringvergelijker toegevoegd

= 1.4.1 =
Upgrade voor een bugfix binnen de Energievergelijker

= 1.4 =
Energievergelijker toegevoegd

= 1.3 =
Wintersportvergelijker toegevoegd

= 1.2 =
Zorgverzekeringvergelijker toegevoegd

= 1.0 =
Stabiele release

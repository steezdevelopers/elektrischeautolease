<?php class ACFFields
{
    // Images
    const BANNER_ID                     = 'banner_id';
    const THUMBNAIL_ID                  = 'thumbnail_id';

    // Blog
    const SHARE_SHOW                    = 'blog_show_share';
    const RELATED_SHOW                  = 'blog_show_related';
    const RELATED_PINNED                = 'blog_related_pinned';


    // Structured data - Organization
    const ORGANIZATION_NAME             = 'organization_name';
    const ORGANIZATION_STREETADDRESS    = 'organization_streetaddress';
    const ORGANIZATION_ADDRESSLOCALITY  = 'organization_addresslocality';
    const ORGANIZATION_ADDRESSREGION    = 'organization_addressregion';
    const ORGANIZATION_POSTALCODE       = 'organization_postalcode';
    const ORGANIZATION_ADDRESSCOUNTRY   = 'organization_addresscountry';
    const ORGANIZATION_LOGO             = 'organization_logo';
    const ORGANIZATION_TELEPHONE        = 'organization_telephone';
    const ORGANIZATION_EMAIL            = 'organization_email';

    const SOCIAL_FACEBOOK               = 'social_facebook';
    const SOCIAL_INSTAGRAM              = 'social_instagram';
    const SOCIAL_LINKEDIN               = 'social_linkedin';
    const SOCIAL_TWITTER                = 'social_twitter';
    const SOCIAL_YOUTUBE                = 'social_youtube';
    const SOCIAL_PINTEREST              = 'social_pinterest';

    // Optionspage
    const CONTACT_NAW                   = 'contact_naw';
    const CONTACT_LOCATION              = 'contact_location';


    const GOOGLE_API_KEY                = 'google_api_key';
    const GOOGLE_SITE_VERIFICATION      = 'google_site_verification';
    const GOOGLE_TAG_MANAGER            = 'google_tag_manager';

    const USPS                          = 'usps';
}

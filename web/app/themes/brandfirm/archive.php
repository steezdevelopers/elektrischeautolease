<?php
/**
 * Template for displaying all single posts
 *
 * @package brandfirm-setup
 */

use Timber\Timber;

$context = Timber::get_context();

//global $wp_query;
//if (!empty($wp_query->posts)) {
//	/** @var WP_Post $post */
//	foreach ($wp_query->posts as $post) {
//		$context = add_terms_to_context('type', $post->ID);
//		$context = add_terms_to_context('lease', $post->ID);
//		$context = add_terms_to_context('seats', $post->ID);
//		$context = add_terms_to_context('brand', $post->ID);
//	}
//}
//
//function add_terms_to_context ($tax_slug, $post_id)
//{
//	$context = Timber::get_context();
//	$types = wp_get_post_terms($post_id, $tax_slug, ['hide_empty' => true]);
//	if (!empty($types)) {
//		/** @var WP_Term $type */
//		foreach ($types as $type) {
//			$context[$tax_slug][] = new Term($type->term_id);
//		}
//	}
//	var_dump($context['type']);
//	return $context;
//}

$context['pagination'] = Timber::get_pagination();
$context['type'] = Timber::get_terms(
	[
		'taxonomy' => 'type',
		'hide_empty' => 'true'
	]
);
$context['leases'] = Timber::get_terms(
	[
		'taxonomy' => 'lease',
		'hide_empty' => 'true',
		'orderby' => 'count',
		'order' => 'DESC',
		'meta_query' => [
			[
				'key' => 'lease_show_in_filter',
				'value' => 1,
			],
		]
	]
);
$context['seats'] = Timber::get_terms(
	[
		'taxonomy' => 'seats',
		'hide_empty' => 'true'
	]
);
$context['brands'] = Timber::get_terms(
	[
		'taxonomy' => 'brand',
		'hide_empty' => 'true'
	]
);

//$context['delivery_times'] = Timber::get_terms(
//	[
//		'taxonomy' => BrandfirmTaxonomies::DELIVERY_TIMES,
//		'hide_empty' => 'true'
//	]
//);

$tax_additions = Timber::get_terms(
	[
		'taxonomy' => BrandfirmTaxonomies::TAX_ADDITION,
		'hide_empty' => 'true',
		'orderby' => 'slug',
		'order' => 'ASC',
	]
);

$sorted_tax_additions = [];

if (!empty($tax_additions)) {
	/** @var WP_Term $tax_addition */
	foreach ($tax_additions as $tax_addition) {
		$sorted_tax_additions[$tax_addition->slug] = $tax_addition;
	}
}

ksort($sorted_tax_additions);

$context['tax_additions'] = $sorted_tax_additions;

if ($tooltip_lease = get_field('tooltip_lease', 'model_archive_options')) {
	$context['tooltips']['lease'] = $tooltip_lease;
}
if ($tooltip_brand = get_field('tooltip_brand', 'model_archive_options')) {
	$context['tooltips']['brand'] = $tooltip_brand;
}
if ($tooltip_price = get_field('tooltip_price', 'model_archive_options')) {
	$context['tooltips']['price'] = $tooltip_price;
}
if ($tooltip_range = get_field('tooltip_range', 'model_archive_options')) {
	$context['tooltips']['range'] = $tooltip_range;
}
if ($tooltip_type = get_field('tooltip_type', 'model_archive_options')) {
	$context['tooltips']['type'] = $tooltip_type;
}
if ($tooltip_seats = get_field('tooltip_seats', 'model_archive_options')) {
	$context['tooltips']['seats'] = $tooltip_seats;
}
if ($tooltip_type = get_field('tooltip_tax_addition', 'model_archive_options')) {
	$context['tooltips']['tax_addition'] = $tooltip_type;
}
if ($tooltip_seats = get_field('tooltip_delivery_time', 'model_archive_options')) {
	$context['tooltips']['delivery_time'] = $tooltip_seats;
}

/**
 * Blog integration
 */
if ('post' === get_post_type()) {
	$context = set_blogs_to_context();
}

Timber::render(array(
	'archive-' . get_post_type() . '.twig',
	'archive.twig'
), $context);

?>

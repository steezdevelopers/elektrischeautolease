jQuery(document).ready(function () {
    jQuery('.js-clicked-affiliate-url').click(function (e){
        e.preventDefault();
        const $this = jQuery(this);
        const href = $this.attr('href');
        const name = 'Clicked the affiliate link ' + href;
        console.log(name);
        ga('send', 'event', 'affiliateButton', 'click', name);
        window.open(href, '_blank');
    });
});

import {Cars} from './widgets/cars';
import {GclidManager} from "./gclid/gclid-manager";
import { Reviews } from './widgets/reviews'

class App {
    constructor() {
        this.widgets = [];
        this.widgetMap = {
            [Cars.NAME]: Cars
        };

        this.addEventListeners();
    }

    initWidgets($container) {
        $container = $container || $(document);

        for (const key in this.widgetMap) {
            const elements = $container[0].querySelectorAll('[data-'.concat(key).concat(']'));

            for (const element of elements) {
                this.attachWidgetToElement(element, key);
            }
        }

        if (location.hostname === 'localhost') {
            console.log(this.widgets);
        }
        ;
    }

    attachWidgetToElement(element, key) {
        for (const widget of this.widgets) {
            if (widget.element === element) {
                return;
            }
        }

        const newWidget = new this.widgetMap[key]($(element));
        newWidget.init();

        this.widgets.push(newWidget);
    }

    clearWidgets($container) {
        $container = $container || $(document);

        for (const key in this.widgetMap) {
            const elements = $container[0].querySelectorAll('[data-'.concat(key).concat(']'));

            for (const element of elements) {
                this.detachWidgetsFromElement(element);
            }
        }
    }

    detachWidgetsFromElement(element) {
        const filteredWidgets = [];

        for (const widget of this.widgets) {
            if (widget.element !== element) {
                filteredWidgets.push(widget);
            }
        }

        this.widgets = filteredWidgets;
    }

    addEventListeners() {
        this.initWidgets();
    }
}

document.addEventListener('DOMContentLoaded', () => {
    new App();
    /**
     * Start the GclidManager for passing of gclid to the form.
     * @type {GclidManager}
     */
    const gclidManager = new GclidManager();
    gclidManager.assignTrackingParameterToCookie('gclid', 'gform');
});

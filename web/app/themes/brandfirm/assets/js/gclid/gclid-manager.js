import {CookieHelper} from "../helpers/cookie-helper";
import {ParameterHelper} from "../helpers/parameter-helper";

/**
 * A manager for gclid management.
 * @see https://zackphilipps.com/store-gclid-cookie-send-to-hubspot/
 */
export class GclidManager {
    /**
     * Instantiate the helpers.
     */
    constructor() {
        this.parameterHelper = new ParameterHelper();
        this.cookieHelper = new CookieHelper();
    }

    /**
     * Assign tracking parameter to a cookie.
     * @param fieldParam
     * @param formType
     */
    assignTrackingParameterToCookie(fieldParam, formType) {
        var field = this.parameterHelper.getParam(fieldParam),
            inputs;
        if (field) {
            this.cookieHelper.setCookie(fieldParam, field, 365);
        }
        if (formType == 'gform') {
            inputs = document.querySelectorAll('.' + fieldParam + ' input[type="text"]');
            this.assignCookieValueToFormInput(fieldParam, inputs);
        } else if (formType == 'hubspot') {
            inputs = document.querySelectorAll('.hs-input[name="' + fieldParam + '"]');
            this.assignCookieValueToFormInput(fieldParam, inputs);
        }
    }

    /**
     * Assign the cookie value to the gform input.
     * @param fieldParam
     * @param inputs
     */
    assignCookieValueToFormInput(fieldParam, inputs) {
        const field = this.cookieHelper.getCookie(fieldParam),
            length = inputs.length;
        if (field && length) {
            for (let i = 0; i < length; i++) {
                inputs[i].value = field;
            }
        }
    }
}

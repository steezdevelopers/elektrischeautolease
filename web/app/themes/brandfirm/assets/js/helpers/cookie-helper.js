/**
 * A cookie helper to set and get cookies.
 * @see https://zackphilipps.com/store-gclid-cookie-send-to-hubspot/
 */
export class CookieHelper {
    /**
     * Set a cookie and name its days.
     * @param name
     * @param value
     * @param days
     */
    setCookie(name, value, days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        var expires = '; expires=' + date.toGMTString();
        document.cookie = name + '=' + value + expires + ';path=/';
    }

    /**
     * Get a cookie by its name.
     * @param name
     * @returns {string}
     */
    getCookie(name) {
        var value = '; ' + document.cookie;
        var parts = value.split('; ' + name + '=');
        if (parts.length == 2)
            return parts.pop().split(';').shift();
    }
}

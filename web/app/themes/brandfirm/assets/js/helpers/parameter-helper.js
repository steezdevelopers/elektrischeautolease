/**
 * Helper for parameters in the URL.
 * @see https://zackphilipps.com/store-gclid-cookie-send-to-hubspot/
 */
export class ParameterHelper {
    /**
     * Get a parameter by its key.
     * @param p
     * @returns {string}
     */
    getParam(p) {
        var match = RegExp('[?&]' + p + '=([^&]*)').exec(window.location.search);
        return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
    }
}

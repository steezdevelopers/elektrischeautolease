(function ($) {

    var stHeight = jQuery(".slick-track").height();
    jQuery(".slick-slide .benefit--card").css("height", stHeight + "px");

    jQuery(document).ready(function () {

        if (jQuery(".faq--container")) {
            var acc = document.getElementsByClassName("faq__question");
            var i;
            for (i = 0; i < acc.length; i++) {
                acc[i].addEventListener("click", function () {
                    this.classList.toggle("active");
                    var panel = this.nextElementSibling;
                    if (panel.style.maxHeight) {
                        panel.style.maxHeight = null;
                    } else {
                        panel.style.maxHeight = panel.scrollHeight + "px";
                    }
                });
            }
        }
        jQuery("#menu--toggle").click(function () {
            jQuery("body").toggleClass("menu-open");
            jQuery(this).toggleClass("active");
            jQuery("nav").toggleClass("active");
            jQuery(".header__search").removeClass("active");
        });
        jQuery("#search--toggle").click(function () {
            jQuery("nav").removeClass("active");
            jQuery("#menu--toggle").removeClass("active");
            jQuery(".header__search").toggleClass("active");
        });
        jQuery(".filter--toggle, .filter__button-container").click(function () {
            jQuery(".overlay").toggleClass("active");
            jQuery(".index--summary").toggleClass("active");
            jQuery(".filter__button-container").toggleClass(
                "filter__button-container--active"
            );
        });
        jQuery(".overlay").click(function () {
            jQuery(this).toggleClass("active");
            jQuery(".index--summary").toggleClass("active");
            jQuery(".filter__button-container").toggleClass(
                "filter__button-container--active"
            );
        });

        jQuery(".close--filter").click(function () {
            jQuery(".overlay").toggleClass("active");
            jQuery(".index--summary").toggleClass("active");
            jQuery(".filter__button-container").toggleClass(
                "filter__button-container--active"
            );
        });

        jQuery('iframe[src*="youtube.com"]').wrap("<div class='video--wrapper'/>");
        jQuery("#back-to-top").click(function () {
            jQuery("html, body").animate({scrollTop: 0}, "slow");
            return false;
        });
        jQuery('a[href^="#"]').click(function () {
            var target = jQuery(this).attr("href");
            jQuery("html, body").animate(
                {scrollTop: jQuery(target).offset().top - 100},
                800
            );
            return false;
        });
        if (jQuery(window).width() <= 768) {
            jQuery(".header__nav ul li > a.nav__link--dropdown svg").click(function (
                e
            ) {
                if (jQuery(this).parent().parent().is(".show")) {
                    console.log("close this");
                    e.preventDefault();
                    jQuery(this).parent().parent().removeClass("show");
                } else {
                    e.preventDefault();
                    console.log("open this");
                    jQuery(this).parent().parent().addClass("show");
                }
            });
            jQuery("#mobile--menu li.menu-item-has-children span.arrow").click(
                function (e) {
                    jQuery(this).parent().toggleClass("show");
                }
            );
        }

        /**
         * Trigger the lease switch based on a get parameter
         */
        if ($("body").hasClass("single-models")) {
            const $leaseType = $(".js-lease-type");
            const $leaseSwitch = $('.js-lease-switch');
            const $leaseTitle = $('.js-lease-title');
            const $leasePrice = $('.js-lease-price');
            const $ogButton = $(".js-og-button");

            /**
             * Check the form state
             */
            if ($leaseSwitch.prop('checked') && 'business' === $leaseType.val()) {
                // This state is: "check" is private, but data is for business.
                // This is incorrect, so set data to business.
                switchLease();
                let label = $leaseSwitch.data('label');
                let price = $leaseSwitch.data('price');
                $leaseTitle.html(label);
                $leasePrice.html(price);
            }

            /**
             * Submit the configurator form.
             */
            $ogButton.click(function (e) {
                e.preventDefault();
                const $this = jQuery(this);
                const $configuratorForm = $("#configurator");

                let baseUrl = $("#quote_page_business").val();

                if ("private" === $leaseType.val()) {
                    baseUrl = $("#quote_page_private").val();
                }
                const params = $configuratorForm.serialize();
                window.location.href = baseUrl + "?" + params + '&occasion=' + $this.data('occasion');
            });

            /**
             * Toggle the business class
             */
            $leaseSwitch.on("click", function () {
                switchLease();
            });

            if (findGetParameter("type") === "private") {
                // Switch toggle to private
                $leaseSwitch.trigger("click");
            }
        }
    });

    jQuery(window).scroll(function () {
        var scroll = jQuery(window).scrollTop();
        if (scroll >= 60) {
            jQuery(".site__header").addClass("scroll");
        } else {
            jQuery(".site__header").removeClass("scroll");
        }
    });

    jQuery(window).on("resize", function () {
        if (jQuery(window).width() > 768) {
            jQuery("nav.header__nav, #menu--toggle").removeClass("active");
            jQuery("body").removeClass("menu-open");
        }
    });

    if (jQuery(".carousel")) {
        jQuery(".carousel .carousel__wrapper").slick({
            infinite: true,
            slidesToShow: 1,
            arrows: false,
            slidesToScroll: 1,
            mobileFirst: true,
            dots: true,
            autoplay: true,
            autoplaySpeed: 3000,
            dots: true,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1,
                        infinite: true,
                        dots: true,
                    },
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                    },
                },
            ],
        });
    }

    if (jQuery(".carousel-image__list")) {
        jQuery(".carousel-image").slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            fade: false,
            asNavFor: ".carousel-image__list",
            adaptiveHeight: true
        });
        jQuery(".carousel-image__list").slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            asNavFor: ".carousel-image",
            dots: false,
            focusOnSelect: true,
            mobileFirst: true,
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 4,
                    },
                },
            ],
        });
    }

    let carsSectionInView = function () {
        const dataCarsSection = document.querySelector("div[data-cars-section]");

        if (!dataCarsSection) {
            return false; // bail
        }

        const bounding = dataCarsSection.getBoundingClientRect();
        let inView = false;

        if (
            bounding.top <=
            (window.innerHeight || document.documentElement.clientHeight) &&
            bounding.bottom >= 121 // height of mobile header
        ) {
            inView = true;
        }

        return inView;
    };

    let handleFilterVisibility = function () {
        let inView = carsSectionInView();
        const $filterToggle = jQuery(".filter--toggle");
        let className = "filter--toggle--sticky";

        if (inView && !$filterToggle.hasClass(className)) {
            $filterToggle.addClass(className);
        }

        if (!inView && $filterToggle.hasClass(className)) {
            $filterToggle.removeClass(className);
        }
    };

    window.addEventListener("scroll", function (event) {
        handleFilterVisibility();
    });
    handleFilterVisibility();

    $(".view-more").on("click", function (e) {
        e.preventDefault();
        $(this)
            .siblings(".filter__checkbox")
            .removeClass("filter__checkbox--hidden");
        $(this).addClass("view-more--hidden");
        $(this).siblings(".view-less").addClass("view-less--active");
    });

    $(".view-less").on("click", function (e) {
        e.preventDefault();
        $(this)
            .siblings(".filter__checkbox--maybe-hidden")
            .addClass("filter__checkbox--hidden");
        $(this).removeClass("view-less--active");
        $(this).siblings(".view-more").removeClass("view-more--hidden");
    });

    $('.js-custom-submit-button').click(function (e) {
        e.preventDefault();
        let $submitButton = $('.gform_wrapper form .gform_button');
        $submitButton.trigger('click');
    });

    const StickyButton = (() => {
        let settings = {};

        const defaults = {
            ogButton: document.querySelector(".js-og-button"),
            stickyButton: document.querySelectorAll(".js-sticky-container"),
            stickyClass: "is-sticky",
            headerHeight: document.querySelector(".site__header").offsetHeight,
        };

        const _checkScroll = () => {
            const rootMargin = `-${settings.headerHeight}px 0px 0px 0px`;
            const _observer = new IntersectionObserver(
                (entries) => {
                    entries.forEach((entry) => {
                        if (!entry.isIntersecting) {
                            settings.stickyButton[0].classList.add(settings.stickyClass);
                        }

                        if (entry.isIntersecting) {
                            settings.stickyButton[0].classList.remove(settings.stickyClass);
                        }
                    });
                },
                {rootMargin}
            );

            _observer.observe(settings.ogButton);
        };

        const _eventListener = () => {
            document.addEventListener("scroll", () => {
                if (
                    window.scrollY >
                    settings.ogButton.offsetTop - settings.headerHeight
                ) {
                    _checkScroll();
                } else {
                    settings.stickyButton[0].classList.remove(settings.stickyClass);
                }
            });
        };

        const init = (options) => {
            // Setup settings
            options = options || {};
            settings = Object.assign({}, defaults, options);

            // If original button doesn't exist return otherwise errors
            if (!settings.ogButton) return;
            _eventListener();
        };

        return {
            init,
        };
    })();

    StickyButton.init();

    const ToggleLease = (() => {
        let settings = {};

        const defaults = {
            switch: document.querySelector(".js-lease-switch"),
            title: document.querySelector(".js-lease-title"),
            price: document.querySelector(".js-lease-price"),
            stickyPrice: document.querySelector(".js-sticky-price"),
        };

        const _switchLease = (event) => {
            const target = event.currentTarget;
            const newLabel = target.getAttribute("data-label");
            const oldLabel = settings.title.innerHTML;
            const newPrice = target.getAttribute("data-price");
            const oldPrice = settings.price.innerHTML;

            settings.title.innerHTML = newLabel;
            target.setAttribute("data-label", oldLabel);
            settings.price.innerHTML = newPrice;
            target.setAttribute("data-price", oldPrice);
            settings.stickyPrice.innerHTML = newPrice;
        };

        const _eventListener = () => {
            settings.switch.addEventListener("click", _switchLease);
        };

        const init = (options) => {
            // Setup settings
            options = options || {};
            settings = Object.assign({}, defaults, options);

            if (settings.stickyPrice) {
                settings.stickyPrice.innerHTML = settings.price.innerHTML;
            }

            // If original button doesn't exist return otherwise errors
            if (!settings.switch) return;
            _eventListener();
        };

        return {
            init,
        };
    })();

    ToggleLease.init();
})(jQuery);

/**
 * @param parameterName
 * @returns {null}
 */
function findGetParameter(parameterName) {
    var result = null,
        tmp = [];
    location.search
        .substr(1)
        .split("&")
        .forEach(function (item) {
            tmp = item.split("=");
            if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
        });
    return result;
}

/**
 * Switch the hidden lease field
 */
function switchLease() {
    const $hiddenField = jQuery(".js-lease-type");
    const val = $hiddenField.val();
    $hiddenField.val(val === "business" ? "private" : "business");
}

/* =====
    New Homepage (N0name Developer)
    ====*/

//Tabs
function openForm(evt, cityName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
}

//Accordions
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.display === "block") {
      panel.style.display = "none";
      panel.classList.toggle("active");
    } else {
      panel.style.display = "block";
      panel.classList.toggle("active");
    }
  });
}

//leasing Form
jQuery(document).ready(function ($) {
    
    //Banner Auto Top Padding from header
    function banner_top_spacing(){
        var headerOuterHeight = $('header.site__header').outerHeight();

        $('main.site__main > .section--banner_home').css('padding-top', headerOuterHeight+ 30 +'px');
    }
    banner_top_spacing();
    window.addEventListener('resize',banner_top_spacing);

    /* Banner Heading */
    $('.section--banner_home .content__item h1').each(function (index, element) {
        var heading = $(element), word_array, last_word, first_part;

        word_array = heading.html().split(/\s+/); // split on spaces
        last_word = word_array.pop();             // pop the last word
        first_part = word_array.join(' ');        // rejoin the first words together

        heading.html([first_part, ' <span>', last_word, '<svg width="271" height="22" viewBox="0 0 271 22" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M2.5 19C52.4232 7.87268 175.516 -8.50674 268.5 14.9942" stroke="#04BC83" stroke-width="5" stroke-linecap="round"/></svg></span>'].join(''));
    });

    //Car Lease Form
    $(".lease_form_wrap form").on("submit", function (event) {

        event.preventDefault();

        //Form with fields
        const form = $(this);
        const redirectLinkObj = form.find('input[name=eal_redirect_link]');

        if (redirectLinkObj.val()) {
            const redirectUrl = new URL(redirectLinkObj.val());
            const formValues = form.serializeArray();

            $.each(formValues, function (i, field) {
                if (field.value !== 'init' && field.name.substr(0,5) === 'quick') {
                    redirectUrl.searchParams.append(field.name, field.value);
                }
            });

            window.location.href = redirectUrl.href;
        }
    });

    //Posts Slider
    jQuery('.blog--posts-slider').slick({
        infinite: true,
        slidesToShow: 1,
        arrows: true,
        slidesToScroll: 1,
        mobileFirst: true,
        dots: false,
        autoplay: true,
        autoplaySpeed: 3000,

        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                },
            },
            {
                breakpoint: 580,
                settings: {
                    slidesToShow: 2,
                },
            },
        ],
    });

    //Car Types Slider
    jQuery('.car_type__slider').slick({
        infinite: true,
        slidesToShow: 1,
        arrows: true,
        slidesToScroll: 1,
        mobileFirst: true,
        dots: true,
        /*autoplay: true,
        autoplaySpeed: 3500,*/

        responsive: [
        {
            breakpoint: 1199,
            settings: {
                slidesToShow: 5,
            },
        },
        {
            breakpoint: 1024,
            settings: {
                slidesToShow: 4,
            },
        },
        {
            breakpoint: 767,
            settings: {
                slidesToShow: 3,
            },
        },
        {
            breakpoint: 380,
            settings: {
                slidesToShow: 2,
            },
        },
        ],
    });

    //Usps list next section padding from top
    function usps_next_section(){
        if($('.section--banner_home').find('.usps--inline')){
            if(window.matchMedia("(min-width: 768px)").matches){
                $('.section--banner_home').next('.section').css('padding-top','6.4rem');

            }else{
                $('.section--banner_home').next('.section').css('padding-top','9rem');
            }
        }
    }
    usps_next_section();
    window.addEventListener('resize',usps_next_section);

    //Equal Height
    function equal_height($parent, $childs){
        $($parent).each(function(){  
            var highestBox = 0;
            $($childs, this).each(function(){
                if($(this).height() > highestBox) {
                    highestBox = $(this).height(); 
                }
            });  
            $($childs,this).height(highestBox);
        }); 
    }

    //Equal Height For Blog Posts
    equal_height('.blog--posts-slider', '.news--card');

    //Equal Height For Car listings
    equal_height('.related_carousel--cars__list', '.related--cars__item');
    window.addEventListener('resize',equal_height);

});

import { BaseWidget } from './base-widget';

export class Banner extends BaseWidget {
    static NAME = 'banner';

    constructor(element) {
        super(element);
    }

    init() {
        console.log('banner');
    }
}

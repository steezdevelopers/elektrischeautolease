$(".carousel-container").slick({
    infinite: true,
    slidesToShow: 2,
    arrows: false,
    slidesToScroll: 1,
    mobileFirst: true,
    dots: false,
    autoplay: true,
    autoplaySpeed: 3000,
    appendArrows: $('.arrows'),
    prevArrow: `
        <button class="carousel__arrow carousel__arrow--prev">
            <span class="carousel__arrow-text">Previous</span>
            <span class="carousel__arrow-icon carousel__arrow-icon--prev"></span>
        </button>`,
    nextArrow: `
        <button class="carousel__arrow carousel__arrow--next">
            <span class="carousel__arrow-text">Next</span>
            <span class="carousel__arrow-icon carousel__arrow-icon--next"></span>
        </button>`,
    responsive: [
        {
            breakpoint: 1024,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 3,
                infinite: true,
                dots: true
            }
        },
        {
            breakpoint: 600,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2
            }
        }
});

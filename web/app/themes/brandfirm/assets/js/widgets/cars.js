import {BaseWidget} from './base-widget';
import noUiSlider from "nouislider";

export class Cars extends BaseWidget {
    static NAME = 'cars';

    constructor(element) {
        super(element);

        this.siteUrl = this.element.data('site');
        this.ajaxurl = customUrls.ajaxUrl;

        this.brand_ids = this.element.data('brand-ids');

        this.type_ids = [];
        this.seat_ids = [];
        this.tax_addition_ids = [];
        this.delivery_time_ids = [];
        this.lease_ids = [];

        /** The default brand list */
        this.brand_ids_archive = [];

        this.checkboxBrand = $('[data-input-brand]', this.element);
        this.checkboxType = $('[data-input-type]', this.element);
        this.checkboxSeats = $('[data-input-seat]', this.element);
        this.checkboxLease = $('[data-input-lease]', this.element);
        this.checkboxDeliveryTime = $('[data-input-delivery_time]', this.element);
        this.checkboxTaxAdditions = $('[data-input-tax_addition]', this.element);

        /**
         * Init the archive sorter.
         * @type {HTMLElement}
         */
        this.archiveSorting = $('#archive-sorting');
        this.initialCount = $('#archive-sorting-count .count').html();

        /**
         * Initialize the noUiSlider.
         * @type {HTMLElement}
         */
        this.slider = document.getElementById('car-range-slider');
        this.price_slider = document.getElementById('price-slider');
        this.contract_slider = document.getElementById('contract-slider');
        this.km_per_year_slider = document.getElementById('km-per-year-slider');

        if (this.slider && this.price_slider && this.contract_slider && this.km_per_year_slider) {
            /**
             * Fix everything for the car range slider
             */
            this.initSliders();
            this.getPips(this, this.slider);
            this.getPips(this, this.price_slider);
            this.getPips(this, this.contract_slider);
            this.getPips(this, this.km_per_year_slider);

            this.addEventTriggers(this.slider);
            this.addEventTriggers(this.price_slider);
            this.addEventTriggers(this.contract_slider);
            this.addEventTriggers(this.km_per_year_slider);

            this.resetCurrentRange(this.slider);
            this.resetCurrentRange(this.price_slider);
            this.resetCurrentRange(this.contract_slider);
            this.resetCurrentRange(this.km_per_year_slider);
        }

        /**
         * Check if lease ids are set before load anyway
         */
        if ($('body').hasClass('tax-lease') && this.checkboxLease.data('lease-id')) {
            this.lease_ids.push(this.checkboxLease.data('lease-id'));
        }

        this.setValuesFromGet();
    }

    init() {

        this.checkboxBrand.on('change', (e) => {
            let brandId = $(e.target).data('brand-id');

            if (this.brand_ids_archive.includes(brandId)) {
                this.brand_ids_archive.splice($.inArray(brandId, this.brand_ids_archive), 1);
            } else {
                this.brand_ids_archive.push(brandId);
            }

            this.getPosts();
        });

        this.checkboxType.on('change', (e) => {
            let typeId = $(e.target).data('type-id');

            if (this.type_ids.includes(typeId)) {
                this.type_ids.splice($.inArray(typeId, this.type_ids), 1);
            } else {
                this.type_ids.push(typeId);
            }
            this.getPosts();
        });

        this.checkboxSeats.on('change', (e) => {
            let seatId = $(e.target).data('seat-id');

            if (this.seat_ids.includes(seatId)) {
                this.seat_ids.splice($.inArray(seatId, this.seat_ids), 1);
            } else {
                this.seat_ids.push(seatId);
            }

            this.getPosts();
        });

        this.checkboxTaxAdditions.on('change', (e) => {
            let taxAdditionId = $(e.target).data('tax_addition-id');

            if (this.tax_addition_ids.includes(taxAdditionId)) {
                this.tax_addition_ids.splice($.inArray(taxAdditionId, this.tax_addition_ids), 1);
            } else {
                this.tax_addition_ids.push(taxAdditionId);
            }

            this.getPosts();
        });

        this.checkboxDeliveryTime.on('change', (e) => {
            let deliveryTimeId = $(e.target).data('delivery_time-id');

            if (this.delivery_time_ids.includes(deliveryTimeId)) {
                this.delivery_time_ids.splice($.inArray(deliveryTimeId, this.delivery_time_ids), 1);
            } else {
                this.delivery_time_ids.push(deliveryTimeId);
            }

            this.getPosts();
        });

        this.checkboxLease.on('change', (e) => {
            let leaseId = $(e.target).data('lease-id');

            if (this.lease_ids.includes(leaseId)) {
                this.lease_ids.splice($.inArray(leaseId, this.lease_ids), 1);
            } else {
                this.lease_ids.push(leaseId);
            }

            this.getPosts();
        });

        this.archiveSorting.on('change', (e) => {
            this.getPosts();
        });
    }

    getPosts() {
        $('.loading-bar').addClass('is-loading');

        let carRangeSlider = this.slider;
        let priceSlider = this.price_slider;
        let contractSlider = this.contract_slider;
        let kmPerYearSlider = this.km_per_year_slider;

        let archiveSorting = this.archiveSorting;

        /** Default data object */
        let data = {
            action: 'filter_cars',
            mode: 'POST',
            brand_ids: this.brand_ids_archive,
            type_ids: this.type_ids,
            seat_ids: this.seat_ids,
            lease_ids: this.lease_ids,
            tax_addition_ids: this.tax_addition_ids,
            delivery_time_ids: this.delivery_time_ids,
            meta_key: 'price',
            range_filter: carRangeSlider.noUiSlider.get(),
            price_filter: priceSlider.noUiSlider.get(),
            order_by: archiveSorting.val(),
            initialCount: this.initialCount
        };

        if($('body').hasClass('tax-brand')){
            let term_id = $('input.data-input-term').val();
            data.term_id = term_id;
            data.request_type = 'brand';
        }

        if($('body').hasClass('tax-lease')){
            let term_id = $('input.data-input-term').val();
            data.term_id = term_id;
            data.request_type = 'private';
            data.contract_filter = contractSlider.noUiSlider.get();
            data.km_per_year_filter = kmPerYearSlider.noUiSlider.get();
        }

        if ($('body').hasClass('tax-modellandingspage')) {
            data['modellandingspage'] = $('input.hidden-term-id').val();
        }

        $.ajax({
            type: 'POST',
            url: this.siteUrl + this.ajaxurl,
            data: data,
            success: function (response) {
                $('[data-cars-section]').html(response.html);
                let count = response.count;
                $('#archive-sorting-count .count').html(count);
                $('[data-loading-bar]').removeClass('is-loading');
            },
            error: function (exception) {
                $('[data-loading-bar]').removeClass('is-loading');
                console.log('fail', exception);
            }
        });
    }

    /**
     * Initialize the car range slider with pips.
     */
    initSliders() {
        /** Actieradius slider */
        let sliderArgs = {
            connect: true,
            start: [0, 1000],
            step: 100,
            behaviour: 'drag',
            range: {
                'min': 0,
                'max': 1000
            },
            pips: {
                mode: 'count',
                values: 5,
                density: 250
            }
        };
        noUiSlider.create(this.slider, sliderArgs);

        /** Price slider */
        let priceSliderArgs = sliderArgs;
        priceSliderArgs.range = {
            'min': 0,
            'max': 2000
        };
        priceSliderArgs.start = [0, 2000];
        noUiSlider.create(this.price_slider, priceSliderArgs);

        /** 
         * Contract slider
         **/
        let contractSliderArgs = sliderArgs;
        contractSliderArgs.range = {
            'min': 0,
            'max': 72
        };
        contractSliderArgs.step = 12;
        contractSliderArgs.start = [0, 72];
        contractSliderArgs.pips = {
            mode: 'count',
            values: 7,
            density: 12
        }
        noUiSlider.create(this.contract_slider, contractSliderArgs);

        /**
         * Km per year slider
         **/
        let kmPerYearSlider = sliderArgs;
        kmPerYearSlider.range = {
            'min': 0,
            'max': 60000
        };
        kmPerYearSlider.step = 10000;
        kmPerYearSlider.start = [0, 60000];
        kmPerYearSlider.pips = {
            mode: 'steps',
            values: 7,
            density: 7
        }
        noUiSlider.create(this.km_per_year_slider, kmPerYearSlider);
    }

    /**
     * Set the pips below the slider.
     * @see https://refreshless.com/nouislider/examples/#section-click-pips
     */
    getPips(parentObject, slider) {

        let sliderPips = slider.querySelectorAll('.noUi-value');

        function crsClickOnPip() {
            let value = Number(this.getAttribute('data-value'));
            slider.noUiSlider.set(value);
            parentObject.triggerAjaxRequest();
        }

        for (let i = 0; i < sliderPips.length; i++) {
            // For this example. Do this in CSS!
            sliderPips[i].style.cursor = 'pointer';
            sliderPips[i].addEventListener('click', crsClickOnPip);
        }
    }

    /**
     * Set values from the get parameter if necessary.
     */
    setValuesFromGet(){
        const quickFormBrand = findGetParameter('quick_form_brand');
        const quickFormActieradius = findGetParameter('quick_form_actieradius');
        const quickFormBudget = findGetParameter('quick_form_budget');
        const quickFormType = findGetParameter('quick_form_type');

        if (quickFormBrand) {
            this.brand_ids_archive.push(parseInt(quickFormBrand));
            $(`input[data-brand-id=${quickFormBrand}]`).prop('checked', 'true');
        }
        if (quickFormBudget) {
            this.price_slider.noUiSlider.set([null, parseInt(quickFormBudget)]);
            this.updateCurrentRange(this.price_slider);
        }
        if (quickFormActieradius) {
            this.slider.noUiSlider.set([null, parseInt(quickFormActieradius)]);
            this.updateCurrentRange(this.slider);
        }
        if (quickFormType) {
            this.type_ids.push(parseInt(quickFormType));
            $(`input[data-type-id=${quickFormType}]`).prop('checked', 'true');
        }
        if (quickFormBrand || quickFormActieradius || quickFormBudget || quickFormType) {
            this.getPosts();
        }
    }

    /**
     * Add the event triggers for the noUiSlider
     */
    addEventTriggers(slider) {
        slider.noUiSlider.on('end', (e) => {
            this.updateCurrentRange(slider);
            this.triggerAjaxRequest();
        });
    }

    updateCurrentRange(slider) {
        if (slider.length < 1) {
            return;
        }
        const sliderValues = slider.noUiSlider.get();
        $('#js-' + slider.id + ' #current-range__start').html(Math.round(sliderValues[0]));
        $('#js-' + slider.id + ' #current-range__end').html(Math.round(sliderValues[1]));
    }

    resetCurrentRange(slider) {
        const firstValue = $(`#${slider.id} .noUi-value`).first().html();
        const lastValue = $(`#${slider.id} .noUi-value`).last().html();

        $('#js-' + slider.id + ' #current-range__start').html(firstValue);
        $('#js-' + slider.id + ' #current-range__end').html(lastValue);
    }

    /**
     * Trigger the ajax request.
     * @param values The min and max values of the slider in a list.
     * @todo Throttle maybe?
     */
    triggerAjaxRequest(values) {
        this.getPosts();
    }
}

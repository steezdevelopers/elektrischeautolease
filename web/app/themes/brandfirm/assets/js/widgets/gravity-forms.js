import 'jquery-validation';
import './base-widget';
export class GravityForms extends BaseWidget {
    static NAME = 'gform';

    constructor(element) {
        super(element);
    }

    init() {
        // Fix for form submit.
        const formId = this.element.attr('id').split('_').slice(-1).pop();
        // console.log(formId);
        // console.log(this.element.find('input'));

        this.element.find('input').on('change',  event => {
            // console.log('CHANGE');
            (window)['gf_submitting_' + formId] = false;
        });

        // prevent double click form submission
        this.element.submit(function () {
            if ($(this.element).valid()) {
                (window)['gf_submitting_' + formId] = false;
            } else {
                (window)['gf_submitting_' + formId] = true;
            }
        });

        // Don't show valid class on non-required fields
        this.element
            .find('.gform_row').not('.gfield_contains_required')
            .find('input, textarea, select')
            .on('change blur keyup click', function() {
                $(this).closest('li.gform_row').removeClass('is-valid');
            })
        ;

        // this.element.find('.gform_row--email').find('input').rules('add', { 
        //     email: true
        // });


        // Validate form
        this.element.validate({
            onfocusout: function (element) {
                this.element(element);
            },
            unhighlight: function(element) {
                // $(element).closest('.gform_row').removeClass('is-valid');
            },
            highlight: function(element) {
                $(element).closest('.gform_row').addClass('is-error');
                $(element).closest('.gform_row').removeClass('is-valid');
            },
            success: function(element) {
                $(element).closest('.gform_row').addClass('is-valid');
                $(element).closest('.gform_row').removeClass('is-error');
                $(element).closest('.error').remove();
            },
            debug: true,
            errorElement: 'span',
            errorClass: 'error',
        });
    }
}

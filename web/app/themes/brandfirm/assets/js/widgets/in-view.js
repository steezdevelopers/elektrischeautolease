import './base-widget';


console.log('in-view');

export class InView extends BaseWidget {
    static NAME = 'in-view';

    constructor(element) {
        super(element);
    }

    init() {
        this.threshold = this.element.data('in-view');

        const config = {
            root: null, // sets the framing element to the viewport
            rootMargin: '0px',
            threshold: this.threshold ? this.threshold : .15
        };
        
        let observer = new IntersectionObserver(function(entries) {
            entries.forEach(function(item) {
                if (item.isIntersecting) {
                    item.target.classList.remove('is-out-view');
                }
            });
        }, config);
    
        if (this.element.offset().top > (window.scrollY + window.innerHeight)) {
            this.element.addClass('is-out-view');
            observer.observe(this.element[0]);
        }
    }
}

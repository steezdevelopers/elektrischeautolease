import { BaseWidget } from './base-widget';

export class Menu extends BaseWidget {
    static NAME = 'menu';
    // public static readonly NAME = 'menu';
    // private trigger: JQuery;
    // private closeButton: JQuery;
    // private content: JQuery;
    // private isOpen: boolean = false;

    constructor(element) {
        super(element);
        this.trigger = $('[data-menu-toggle]');
        this.closeButton = $('[data-menu-close]');
        this.content = $('[data-content]', this.element);
    }

    init() {
        this.trigger.on('click', (e) => {
            if (this.isOpen) {
                this.close();
            } else {
                this.open();
            }
        });

        this.closeButton.on('click', (e) => {
            if (this.isOpen) {
                this.close();
            }
        });

        $(window).on('resize', () => {
            if (this.isOpen) {
                this.close();
            }
        });
    }

    open() {
        this.isOpen = true;
        this.trigger.addClass('is-open');

        requestAnimationFrame(()  => {
            this.element.addClass('is-active');
            $('.site').addClass('is-fixed');

            requestAnimationFrame(()  => {
                // this.element.addClass('is-open');
                this.element.slideDown();
            });
        });

        $(window).on('scroll', () => {
            if (window.scrollY > 800) {
                this.close();
            }
        });
    }

    close() {
        this.isOpen = false;
        this.trigger.removeClass('is-open');

        requestAnimationFrame(()  => {
            this.element.removeClass('is-open');

            requestAnimationFrame(()  => {
                setTimeout((() => {
                    this.element.removeClass('is-active');
                    $('.site').removeClass('is-fixed');
                }), 200);
            });
        });
    }
}

import slick from 'slick-carousel'

if (jQuery(".js-reviews")) {
    
    if(jQuery(".js-reviews--no-image").length > 0) {
        jQuery(".js-reviews .reviews__wrapper").slick({
            infinite: true,
            slidesToScroll: 1,
            slidesToShow: 1,
            mobileFirst: true,
            autoplay: true,
            rows: 0,
            slidesPerRow: 1,
            arrows: true,
            appendArrows: jQuery('.reviews__arrows'),
            prevArrow: `
            <button class="review__arrow review__arrow--prev">
                <span class="review__arrow-icon review__arrow-icon--prev"></span>
            </button>`,
            nextArrow: `
                <button class="review__arrow review__arrow--next">
                    <span class="review__arrow-icon review__arrow-icon--next"></span>
                </button>`,
            responsive: [
                {
                  breakpoint: 1200,
                  settings: {
                    slidesToShow: 3,
                  }
                },
                {
                  breakpoint: 700,
                  settings: {
                    slidesToShow: 2,
                  }
                }
              ]
          });
    } 
    
    else {
        jQuery(".js-reviews .reviews__wrapper").slick({
            infinite: true,
            slidesToScroll: 1,
            slidesToShow: 1,
            mobileFirst: true,
            autoplay: true,
            rows: 0,
            slidesPerRow: 1,
            arrows: true,
            appendArrows: jQuery('.reviews__arrows'),
            prevArrow: `
            <button class="review__arrow review__arrow--prev">
                <span class="review__arrow-icon review__arrow-icon--prev"></span>
            </button>`,
        nextArrow: `
            <button class="review__arrow review__arrow--next">
                <span class="review__arrow-icon review__arrow-icon--next"></span>
            </button>`,
          });
          
    }
}
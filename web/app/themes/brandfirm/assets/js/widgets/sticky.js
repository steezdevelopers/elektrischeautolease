import { StickyObserver } from '@welt/sticky-observer';

import { BaseWidget } from './base-widget';

export class Sticky extends BaseWidget {
    static NAME = 'sticky';

    constructor(element) {
        super(element);
        this.stickyElement = $('[data-sticky-element]', this.element);
    }

    init() {
        console.log('sticky');
        const stickyElement = document.querySelector('article__aside');
        const sticky = new StickyObserver([stickyElement], document.querySelector('article__container'));
    }
}

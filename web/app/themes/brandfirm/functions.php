<?php

new \Timber\Timber();

class BrandfirmSite extends Timber\Site
{
	
	public function __construct ()
	{
		\Timber\Timber::$dirname = array('views');
		
		// add_filter('show_admin_bar', '__return_false');
		
		add_action('after_setup_theme', [$this, 'theme_supports']);
		add_filter('timber/context', [$this, 'add_to_context']);
		add_filter('timber/twig', [$this, 'add_to_twig']);
		add_filter('Timber\PostClassMap', [$this, 'post_class_mapping']);
		
		add_action('init', [$this, 'register_taxonomies']);
		add_action('init', [$this, 'register_posttypes']);
		
		add_action('wp_enqueue_scripts', [$this, 'add_js_scripts']);
		
		
		add_action('init', [$this, 'register_rewrite_rules'], 1000);
		add_filter('login_errors', [$this, 'login_errors']);
		add_filter('login_enqueue_scripts', [$this, 'add_logo_login']);
		add_filter('login_headerurl', [$this, 'login_logo_url']);
		
		add_action('init', [$this, 'display_letters_anchors']);
		add_action('init', [$this, 'fill_by_letter_array']);
		
		add_filter('redirect_canonical', [$this, 'pif_disable_redirect_canonical']);
		
		add_filter('wpseo_metabox_prio', [$this, 'yoasttobottom']);
		
		
		add_filter('rest_endpoints', [$this, 'remove_users_rest_api']);
		
		/**
		 * Filter the taxonomy
		 */
		add_action('pre_get_posts', [$this, 'customise_speakers_taxonomy_archive_display']);
		
		// Actions - admin only
		if (is_admin()) {
			add_action('wp_before_admin_bar_render', [$this, 'alter_admin_bar']);
			add_action('admin_head', [$this, 'change_bar_color']);
			add_action('wp_head', [$this, 'change_bar_color']);
			
			add_filter('acf/settings/save_json', [$this, 'acf_json_save_point']);
			add_filter('acf/settings/load_json', [$this, 'acf_json_load_point']);
		}
		
		include 'inc/maybe-cache.php';
		include 'inc/posts/includes.php';
		include 'inc/acf-field-gravity-forms.php';
		include 'inc/post-types.php';
		include 'inc/taxonomies.php';
		include 'inc/widgets.php';
		include 'inc/query-helper.php';
		include 'inc/gravity-forms.php';
		include 'inc/acf-option-pages.php';
		include 'inc/shortcodes.php';
		include 'inc/format-prices.php';
		include 'inc/sort-by-price.php';
		
		/**
		 * Blog integration
		 */
		include 'inc/posts/get-blog-posts.php';
		
		/**
		 * Init the ModelSingle class.
		 */
		include 'inc/model-single.php';
		include 'inc/model-configurator.php';
		
		/**
		 * Init the Reviews.
		 */
		include 'inc/reviews.php';
		
		/**
		 * Talk with the Mobility Service API.
		 */
		include 'inc/mobility-service/init.php';
		
		/**
		 * Init the filter_cars()
		 */
		include 'inc/filter_cars.php';
		
		if (!class_exists('acf')) {
			add_action('admin_notices', function () {
				echo sprintf(
					'<div class="error"><p>%s not activated. Make sure you activate the plugin in <a href="%s">%s</a></p></div>',
					'Advanced Custom Fields',
					esc_url(admin_url('plugins.php?s=advanced-custom-fields-pro')),
					esc_url(admin_url('plugins.php'))
				);
			});
			
			return;
		} else {
			add_filter('timber/context', [$this, 'add_to_context']);
			include 'acf/acf-fields.php';
		}
		
		
		// Removes unnecessary meta-data actions from 'wp_head'  // TODO: Check if not necessary
		remove_action('wp_head', 'wp_generator');
		remove_action('wp_head', 'wlwmanifest_link');
		remove_action('wp_head', 'rsd_link');
		remove_action('wp_head', 'feed_links', 2);
		remove_action('wp_head', 'feed_links_extra', 3);
		remove_action('wp_head', 'wp_shortlink_wp_head');
		
		add_action('init', [$this, 'stop_loading_wp_embed']);
		
		Routes::map('modellen/page/:pg', function ($params) {
			// $query = 'posts_per_page=2&post_type=blog&paged='.$params['pg'];
			$query = 'posts_per_page=15&post_type=modellen&paged=' . $params['pg'];
			// Routes::load('archive.php', null, $query, 200);
		});
		
		/**
		 * Fix for old plugins in WP 5.5.
		 */
		add_action('admin_enqueue_scripts', [$this, 'add_jquery_migrate'], 10);
		
		/**
		 * Workaround for SSL check in WP All Import Pro
		 * @see https://www.wpallimport.com/documentation/developers/code-snippets/#curl-error-60
		 */
		add_action('http_api_curl', function ($handle, $r, $url) {
			// Disable peer verification to temporarily resolve error 60.
			curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, false);
		}, 10, 3);
		
		/**
		 * Adds the diffuser script
		 */
		include 'inc/diffuser.php';
	}
	
	public function add_jquery_migrate ()
	{
		wp_enqueue_script('jquery-migrate');
	}
	
	public function theme_supports ()
	{
		add_theme_support('post-thumbnails');
		add_theme_support('menus');
		
		// Menus
		register_nav_menus(array(
			'menu_main' => __('Main navigation', 'brandfirm'),
			'header_button' => __('Header button', 'brandfirm'),
			'menu_footer' => __('Footer navigation', 'brandfirm'),
			'menu_copyright' => __('Copyright', 'brandfirm'),
			// Add more when needed
		));
		
		// Additional sizes
		// thumbnail    - 150, 150, true
		// medium       - 300, 300, false
		// large        - 1024, 1024, false
		add_image_size('banner-small', 600, 280, true);
		add_image_size('banner', 1280, 400, true);
		add_image_size('gallery', 1280, 854, true);
		add_image_size('gallery_thumbnail', 430, 240, true);
		add_image_size('brand_archive_image', 580, 250, true);
		add_image_size('lease_company_logo', 360, null, false);
		add_image_size('sticky_blog_item', 1600, 900, true);
		add_image_size('review_item', 874, null, false);
	}
	
	public function add_to_context ($context)
	{
		$context['site'] = $this;
		$context['site_url'] = home_url('/');
		
		/**
		 * Add the assets to the main context
		 */
		$context['assets']['js_url'] = $this->asset('dist/js/app.js');
		$context['assets']['css_url'] = $this->asset('dist/css/app.css');
		
		$context['menu_main'] = new Timber\Menu('menu_main');
		$context['menu_footer'] = new Timber\Menu('menu_footer');
		$context['menu_copyright'] = new Timber\Menu('menu_copyright');
		$context['header_button'] = new Timber\Menu('header_button');
		
		// Count total vacancies post
		$context['total_vacancies'] = $this->total_vacancies();
		
		// ACF structured data
		$context['structured'] = $this->theme_structured();
		
		// ACF structured data
		$context['headerusps'] = $this->theme_headerusps();
		$context['all_cat'] = $this->get_terms();
		
		// Quote link by options
		$context['quote_link'] = $this->get_quote_link();
		
		if (function_exists('yoast_breadcrumb')) {
			$context['breadcrumbs'] = yoast_breadcrumb('<nav id="breadcrumbs" class="main-breadcrumbs">', '</nav>', false);
		}
		
		$context['footer_column1'] = Timber::get_widgets('footer-column1');
		$context['footer_column2'] = Timber::get_widgets('footer-column2');
		$context['footer_column3'] = Timber::get_widgets('footer-column3');
		$context['footer_column4'] = Timber::get_widgets('footer-column4');
		$context['footer_date_year'] = date('Y');
		
		$context['sidebar'] = Timber::get_widgets('sidebar');
		
		/**
		 * Model archive
		 */
		$model_count = wp_count_posts('models')->publish;
		
		if (is_tax()) {
			$queried_object = get_queried_object();
			if ($queried_object instanceof WP_Term) {
				$model_count = $queried_object->count;
			}
		}
		
		$context['model_count'] = $model_count;
		
		$context['model_archive'] = [
			'model_archive_introduction_title' => get_field('model_archive_introduction_title', 'model_archive_options'),
			'model_archive_introduction_text' => get_field('model_archive_introduction_text', 'model_archive_options'),
			'model_archive_faq_title' => get_field('model_archive_faq_title', 'model_archive_options'),
			'model_archive_faq' => get_field('model_archive_faq', 'model_archive_options')
		];
		
		$context['model_archive_faq'] = [
			'@context' => 'https://schema.org',
			'@type' => 'FAQPage',
			'mainEntity' => []
		];
		
		if (have_rows('model_archive_faq', 'model_archive_options')) {
			while (have_rows('model_archive_faq', 'model_archive_options')) {
				the_row();
				$context['model_archive_faq']['mainEntity'][] = [
					'@type' => 'Question',
					'name' => get_sub_field('question'),
					'acceptedAnswer' => [
						'@type' => 'Answer',
						'text' => get_sub_field('answer')
					]
				];
			}
		}
		
		$context['model_archive_faq_json_object'] = json_encode($context['model_archive_faq']);
		
		return $context;
	}
	
	/** Helper to get any theme asset (also reads manifest.json for versioning) */
	function asset ($path)
	{
		$manifest = @json_decode(file_get_contents(__DIR__ . '/dist/manifest.json'), true);
		$versionedPath = 'app/themes/' . get_template() . '/' . $path;
		
		// webpack dev-server fix.
		if (isset($manifest[$versionedPath]) && strpos($manifest[$versionedPath], 'localhost') !== false) {
			return $manifest[$versionedPath];
		}
		
		return isset($manifest[$versionedPath])
			? get_home_url() . $manifest[$versionedPath]
			: get_template_directory_uri() . '/' . $path;
	}
	
	
	function theme_structured ()
	{
		$options = [
			'structured_data' => [
				'organization_name' => get_field(ACFFields::ORGANIZATION_NAME, 'option'),
				'organization_streetaddress' => get_field(ACFFields::ORGANIZATION_STREETADDRESS, 'option'),
				'organization_addresslocality' => get_field(ACFFields::ORGANIZATION_ADDRESSLOCALITY, 'option'),
				'organization_addressregion' => get_field(ACFFields::ORGANIZATION_ADDRESSREGION, 'option'),
				'organization_postalcode' => get_field(ACFFields::ORGANIZATION_POSTALCODE, 'option'),
				'organization_addresscountry' => get_field(ACFFields::ORGANIZATION_ADDRESSCOUNTRY, 'option'),
				'organization_logo' => get_field(ACFFields::ORGANIZATION_LOGO, 'option'),
				'organization_telephone' => get_field(ACFFields::ORGANIZATION_TELEPHONE, 'option'),
				'organization_email' => get_field(ACFFields::ORGANIZATION_EMAIL, 'option'),
				'organization_url' => get_home_url()
			],
			'social' => [
				'facebook' => get_field(ACFFields::SOCIAL_FACEBOOK, 'option'),
				'instagram' => get_field(ACFFields::SOCIAL_INSTAGRAM, 'option'),
				'linkedin' => get_field(ACFFields::SOCIAL_LINKEDIN, 'option'),
				'twitter' => get_field(ACFFields::SOCIAL_TWITTER, 'option'),
				'youtube' => get_field(ACFFields::SOCIAL_YOUTUBE, 'option'),
				'pinterest' => get_field(ACFFields::SOCIAL_PINTEREST, 'option'),
			],
			'usps' => get_field(ACFFields::USPS, 'option'),
		];
		
		return $options;
	}
	
	// Remove user list endpoint from rest api
	
	/**
	 * Get the quote link for the single model page.
	 * @return string
	 */
	public function get_quote_link ()
	{
		$quote_link = '';
		
		if ($quote_link_option = get_field('quote_page', 'options')) {
			$quote_link = add_query_arg([
				'model_id' => get_the_ID()
			], $quote_link_option);
		}
		
		return $quote_link;
	}
	
	public function add_to_twig ($twig)
	{
		$twig->addExtension(new Twig_Extension_StringLoader());
		
		// $twig->addFilter( new Twig_SimpleFilter( 'myfoo', array( $this, 'myfoo' ) ) );
		
		return $twig;
	}
	
	// Forces single error message on login-errors
	
	function remove_users_rest_api ($aryEndpoints)
	{
		if (isset($aryEndpoints['/wp/v2/users'])) {
			unset($aryEndpoints['/wp/v2/users']);
		}
		if (isset($aryEndpoints['/wp/v2/users/(?P<id>[\d]+)'])) {
			unset($aryEndpoints['/wp/v2/users/(?P<id>[\d]+)']);
		}
		
		return $aryEndpoints;
	}
	
	function cc_mime_types ($mimes)
	{
		$mimes['svg'] = 'image/svg+xml';
		
		return $mimes;
	}
	
	public function count ()
	{
		return $this->_query->post_count;
	}
	
	function login_errors ()
	{
		return sprintf(
			__('<strong>ERROR</strong> Invalid username or password.<br /><a href="%s">Lost your password</a>?', 'bf'),
			wp_lostpassword_url()
		);
	}
	
	function add_logo_login ()
	{
		echo sprintf(
			'<style type="text/css">body.login div#login h1 a { background-image: url(%s); background-size: 251px; height: 55px; margin: 30px auto 0; width: 251px; }</style>',
			get_template_directory_uri() . '/dist/images/elektrische-autolease-logo.png'
		);
	}
	
	function login_logo_url ()
	{
		return 'https://www.elektrischeautolease.nl';
	}
	
	function pif_disable_redirect_canonical ($redirect_url)
	{
		if (is_singular()) {
			$redirect_url = false;
		}
		
		return $redirect_url;
	}
	
	/** Map posttypes to functions */
	function post_class_mapping ()
	{
		return [
			BrandfirmPostTypes::POST => '\Post',
			BrandfirmPostTypes::PAGE => '\Post',
			BrandfirmPostTypes::FAQ => '\Page',
		];
	}
	
	function acf_json_save_point ($path)
	{
		$path = get_stylesheet_directory() . '/acf/acf-json';
		
		return $path;
	}
	
	function acf_json_load_point ($paths)
	{
		unset($paths[0]);
		$paths[] = get_stylesheet_directory() . '/acf/acf-json';
		
		return $paths;
	}
	
	function alter_admin_bar ()
	{
		global $wp_admin_bar;
		$wp_admin_bar->remove_menu('wp-logo');
	}
	
	function register_taxonomies ()
	{
		$taxonomies = new BrandfirmTaxonomies();
		$taxonomies->register_all();
	}
	
	function register_posttypes ()
	{
		$types = new BrandfirmPostTypes();
		$types->register_all();
	}
	
	function change_bar_color ()
	{
		if (getenv('ENV') === 'prod') {
			echo '
            <style type="text/css">
                #wpadminbar:hover {
                    background-color: #ec0f37;
                }
            </style>';
		} elseif (getenv('ENV') === 'dev') {
			echo '
            <style type="text/css">
                #wpadminbar {
                    background-color: #fb8c00;
                }
            </style>';
		}
	}
	
	function loadScripts ()
	{
		wp_enqueue_script('slick_min', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js', ['jquery'], '1.9.0');
	}
	
	/** Remove wp embed if no admin */
	function stop_loading_wp_embed ()
	{
		if (!is_admin()) {
			wp_deregister_script('wp-embed');
		}
	}
	
	/** Yoast to bottom */
	function yoasttobottom ()
	{
		return 'low';
	}
	
	// Adds extra rewrite rules
	function register_rewrite_rules ()
	{
		// // Re-adding paged - pages to top, getting priority over CPT's
		// add_rewrite_rule(
		//     '(.?.+?)/page/?([0-9]{1,})/?$',
		//     'index.php?post_type=page&pagename=$matches[1]&paged=$matches[2]',
		//     'top'
		// );
		
		// add_rewrite_rule(
		//     '([^/]*)/?$',
		//     'index.php?pagename=$matches[1]',
		//     'top'
		// );
		
		flush_rewrite_rules();
	}
	
	/**
	 * Quick fix to fix the Ajax filters - ajaxUrl was incorrectly set
	 * @todo Make a sole enqueue file or do it the bedrock way, this is only a quick fix to make it work for Jeroen!
	 */
	function add_js_scripts ()
	{
		/**
		 * Externals
		 */
		wp_enqueue_script('slick_slider', '//cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js', array(), '1.9.0', true);
		
		/**
		 * Custom scripts
		 */
		wp_enqueue_script('app', $this->asset('dist/js/app.js'), array('jquery'), '1.0', true);
		wp_enqueue_script('custom_script', $this->asset('assets/js/main.js'), array('jquery'), '1.0', true);
		wp_enqueue_script('affiliate-ga-event', $this->asset('assets/js/affiliate-ga-event.js'), array('jquery'), '1.0', true);
		
		/**
		 * Global objects
		 */
		wp_localize_script('custom_script', 'my_ajax_object', array('ajax_url' => admin_url('admin-ajax.php')));
		wp_localize_script('custom_script', 'customUrls', ['ajaxUrl' => admin_url('admin-ajax.php')]);
	}
	
	//Customise Speakers taxonomy archive display
	function customise_speakers_taxonomy_archive_display ($query)
	{
		if (($query->is_main_query()) && (is_tax('brand'))) {
			$query->set('post_type', 'models');
		}
	}
}


new BrandfirmSite();

/**
 * Include a template and pass it a model or arguments.
 *
 * Effectively the same as WordPress' get_template_part only allows for passing of arguments & models.
 *
 * Note: When an array is passed it will extract the arguments and make them available for the included template.
 *  When anything else is passed it will just pass the $model.
 *  The $model variable is always available to the included template.
 *
 * @param string|array $template_names The name of the template that should be fetched (or multiple options, uses first find)
 * @param array|mixed $model A model or an array of arguments
 * @param boolean $echo Should output the results
 *
 * @return bool|string file has been included or result if echo is false
 *
 * @see get_template_part()
 */

 if ( ! function_exists( 'ill_get_template_part' ) ) {

	function ill_get_template_part( $template_names, $model = [], $echo = true ) {

		// Append .php as locate template doesn't support that
		$template_names = (array) $template_names;
		foreach ( $template_names as $index => $template_name) {
			$template_names[$index] = $template_name . '.php';
		}

		// Try to find the template
		$path = locate_template( $template_names, false );

		if ( ! empty( $path ) ) {

			if ( ! empty( $model ) && is_array( $model ) ) {
				// Make any additional vars available for the script
				extract( $model );
			}

			// Unset the variables to prevent accidental access
			unset( $template_names );

			// Include the file (buffered if required)
			if ( ! $echo ) {
				ob_start();
			}

			include $path;

			if ( $echo ) {
				return true;
			} else {
				$result = ob_get_contents();
				ob_end_clean();

				return $result;
			}
		} else {
			return false;
		}

	}

}
<?php

// ACF options pages
if ( function_exists('acf_add_options_page') ) {

    acf_add_options_page(array(
        'page_title' => 'Site opties',
        'menu_title' => 'Site opties',
        'menu_slug' => 'site-options',
        'capability' => 'edit_posts',
        'redirect' => false
    ));

}

<?php
namespace Brandfirm\Devkit;
// // Actions - admin only
// if (is_admin()) {
//     add_action('admin_menu',  'alter_admin_bar');
// }

// function alter_admin_bar()
// {
//     global $wp_admin_bar;
//     // $wp_admin_bar->remove_menu('wp-logo');
//     // var_dump('test');
//     // $wp_admin_bar->remove_menu('wpseo-menu');
// }

// <?php
// /* =========================================================================
//    Admin-specific configuration, scripts and handlers
//    ========================================================================= */
namespace Brandfirm;

class Admin extends BrandfirmSite {

	public function init() {
		// Exit now for non-admin requests
		if (!is_admin()) { return; }

        add_action('admin_enqueue_scripts', array($this, 'enqueueAdminAssets'));
	}

	public function enqueueAdminAssets() {
		wp_enqueue_style(Project::$namespace . '-admin', get_stylesheet_directory_uri() . '/css/admin' . Project::$styleSuffix . '.css', array(), null);
	}

	public function enqueueBlockAssets() {
		wp_enqueue_script(Project::$namespace . '-blocks', get_stylesheet_directory_uri() .  '/js/blocks' . Project::$scriptSuffix . '.js', array('wp-blocks', 'wp-i18n', 'wp-element', 'wp-editor'), null);
	}
}

new Admin();
<?php
/**
 * CarFilterSidebar
 *
 * @copyright Copyright © 2022 Steez Webdevelopment. All rights reserved.
 * @author    tommy@steez.nl
 */

namespace Brandfirm\Devkit;


use Timber\Term;
use WP_Post;
use WP_Term;

class CarFilterSidebar
{
	
	/**
	 * @var array
	 */
	private $filters;
	
	/**
	 * Add the filters to the sidebar based on the current query.
	 */
	public function __construct ()
	{
		$this->filters = [];
		add_filter('timber/context', [$this, 'get_sidebar_filters'], 20, 1);
	}
	
	/**
	 * Retrieve the sidebar filters and add them to the context.
	 * @param $context
	 * @return mixed
	 */
	public function get_sidebar_filters ($context)
	{
		global $wp_query;
		if (!empty($wp_query->posts)) {
			$types = [];
			
			/** @var WP_Post $post */
			foreach ($wp_query->posts as $post) {
				$types[] = $this->add_terms_to_filter('type', 'type', $post->ID);
				$lease[] = $this->add_terms_to_filter('lease', 'leases', $post->ID);
				$seats[] = $this->add_terms_to_filter('seats', 'seats', $post->ID);
				$brands[] = $this->add_terms_to_filter('brand', 'brands', $post->ID);
			}
		}
		
		if (!empty($this->filters)) {
			foreach ($this->filters as $key => $value) {
				ksort($value);
				$context[$key] = $value;
			}
		}
		
		return $context;
	}
	
	/**
	 * Extract terms from a post based on the tax slug and post id. Add it to the filters, based on the filters slug.
	 * @param $tax_slug
	 * @param $filter_slug
	 * @param $post_id
	 * @return array
	 */
	public function add_terms_to_filter ($tax_slug, $filter_slug, $post_id)
	{
		$timber_terms = [];
		$terms = wp_get_post_terms($post_id, $tax_slug, ['hide_empty' => true]);
		if (!empty($terms)) {
			/** @var WP_Term $term */
			foreach ($terms as $term) {
				$timber_term = new Term($term->term_id);
				if (empty($this->filters[$filter_slug]) || !in_array($timber_term, $this->filters[$filter_slug])) {
					$this->filters[$filter_slug][$term->slug] = new Term($term->term_id);
				}
			}
		}
		
		return $timber_terms;
	}
}

new CarFilterSidebar();

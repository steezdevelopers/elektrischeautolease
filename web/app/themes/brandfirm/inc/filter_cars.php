<?php

/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the General Public License (GPL 3.0).
 * This license is available through the world-wide-web at this URL:
 * http://opensource.org/licenses/gpl-3.0.en.php
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this module to newer
 * versions in the future.
 *
 * @author      Tommy Kolkman <tommy@steez.nl>
 * @copyright   Copyright (c) 2020 Steez (https://steez.nl)
 * @license     http://opensource.org/licenses/gpl-3.0.en.php General Public License (GPL 3.0)
 *
 */

class FilterCars
{

	public function __construct()
	{
		add_action('pre_get_posts', [$this, 'init_load_cars'], 1);
		add_action('wp_ajax_filter_cars', [$this, 'filter_cars'], 100);
		add_action('wp_ajax_nopriv_filter_cars', [$this, 'filter_cars'], 100);
	}

	/**
	 * @param $query
	 * @return void
	 */
	public function init_load_cars($query)
	{
		// Check if on the admin screen and it's the main query for the "models" post type
		if (!is_admin() && $query->is_main_query() && $query->get('post_type') === 'models' && is_archive()) {

			// Set up tax_query to group by "lease" taxonomy and sort by term_id in DESC order
			$tax_query = array(
				array(
					'taxonomy' => 'lease',
					'field' => 'id',
					'operator' => 'EXISTS', // Only include posts with the "lease" taxonomy
				),
			);
			$query->set('tax_query', $tax_query);
			$query->set('meta_query', array(
				array(
					'key' => 'price',
					'value' => array(1, 4000),
					'compare' => 'BETWEEN',
					'type' => 'NUMERIC'
				)
			));
			$query->set('orderby', array(
				'term_id' => 'ASC'
			));
		}

		return $query;
	}

	/**
	 * Check if we have a private lease request. 
	 */
	private function is_private_lease()
	{
		$is_private_lease = false;

		$lease_ids = !empty($_POST['lease_ids']) ? $_POST['lease_ids'] : [];

		if (in_array(125, $lease_ids) && !in_array(124, $lease_ids)) {
			$is_private_lease = true;
		}

		if (!empty($_POST['request_type']) && $_POST['request_type'] === 'private') {
			$is_private_lease = true;
		}

		return $is_private_lease;
	}

	/**
	 * Get brand ids based on some parameters like:
	 * - Is it a brand page or a regular archive?
	 */
	public function get_brand_ids($post)
	{
		$brand_ids = [];

		/**
		 * Assign the brand ids from our form.
		 */
		if (!empty($post['brand_ids'])) {
			$brand_ids = $post['brand_ids'];
		}

		/** 
		 * Check if this is a brand page.
		 * If so, set the given term as the brand to filter.
		 **/
		if (
			!empty($post['term_id']) &&
			!empty($post['request_type']) &&
			$post['request_type'] === 'brand'
		) {
			$brand_ids = [$_POST['term_id']];
		}

		return $brand_ids;
	}

	/**
	 * Get lease ids based on some parameters like:
	 * - Is it a lease page or a regular archive?
	 */
	public function get_lease_ids($post)
	{
		$lease_ids = [];

		/**
		 * Assign the brand ids from our form.
		 */
		if (!empty($post['lease_ids'])) {
			$lease_ids = $post['lease_ids'];
		}

		/** 
		 * Check if this is a lease page.
		 * If so, set the given term as the lease to filter.
		 **/
		if (
			!empty($post['term_id']) &&
			!empty($post['request_type']) &&
			$post['request_type'] === 'private'
		) {
			$lease_ids = [$_POST['term_id']];
		}

		return $lease_ids;
	}

	/**
	 * The bizarre Brandfirm method of filtering cars.
	 * @todo Refactor this monster.
	 */
	public function filter_cars()
	{
		$types = \BrandfirmPostTypes::MODELS;
		// @todo: Temp fix to show more than 15 models when switching back
		$per_page = 200;
		$paged = get_query_var('paged', 1);

		// $transient_key = base64_encode(json_encode($_POST));
		// if ($cached_data = get_transient($transient_key)) {
		// 	$response = json_decode(base64_decode($cached_data));
		// 	wp_send_json($response);
		// 	wp_die();
		// } else {

		$args = new QueryArguments($types, $per_page, $paged);

		$brand_ids = $this->get_brand_ids($_POST);
		$lease_ids = $this->get_lease_ids($_POST);
		$brand_ids_archive = !empty($_POST['brand_ids_archive']) ? $_POST['brand_ids_archive'] : [];
		$type_ids = !empty($_POST['type_ids']) ? $_POST['type_ids'] : [];
		$seat_ids = !empty($_POST['seat_ids']) ? $_POST['seat_ids'] : [];
		$delivery_time_ids = !empty($_POST['delivery_time_ids']) ? $_POST['delivery_time_ids'] : [];
		$tax_addition_ids = !empty($_POST['tax_addition_ids']) ? $_POST['tax_addition_ids'] : [];
		$modellandingspage = !empty($_POST['modellandingspage']) ? $_POST['modellandingspage'] : [];

		// Add brand ids
		if (is_array($brand_ids) && !empty($brand_ids)) {
			$args->add_or_tax_query(new QueryTaxonomyFilter(BrandfirmTaxonomies::BRAND, $brand_ids));
		}

		// Add brand ids from the archive /elektrische-autos
		if (is_array($brand_ids_archive) && !empty($brand_ids_archive)) {
			$args->add_or_tax_query(new QueryTaxonomyFilter(BrandfirmTaxonomies::BRAND, $brand_ids_archive));
		}

		// Add types
		if (is_array($type_ids) && !empty($type_ids)) {
			$args->add_or_tax_query(new QueryTaxonomyFilter(BrandfirmTaxonomies::TYPE, $type_ids));
		}

		// Add seats
		if (is_array($seat_ids) && !empty($seat_ids)) {
			$args->add_or_tax_query(new QueryTaxonomyFilter(BrandfirmTaxonomies::SEATS, $seat_ids));
		}

		// Add delivery_time_ids
		if (is_array($delivery_time_ids) && !empty($delivery_time_ids)) {
			$args->add_or_tax_query(new QueryTaxonomyFilter(BrandfirmTaxonomies::DELIVERY_TIMES, $delivery_time_ids));
		}

		// Add tax_addition_ids
		if (is_array($tax_addition_ids) && !empty($tax_addition_ids)) {
			$args->add_or_tax_query(new QueryTaxonomyFilter(BrandfirmTaxonomies::TAX_ADDITION, $tax_addition_ids));
		}

		if (is_array($lease_ids) && !empty($lease_ids)) {
			$args->add_and_tax_query(new QueryTaxonomyFilter(BrandfirmTaxonomies::LEASE, $lease_ids));
		}

		if (!empty($modellandingspage)) {
			$args->add_and_tax_query(new QueryTaxonomyFilter(BrandfirmTaxonomies::MODELLANDINGSPAGE, [$modellandingspage]));
		}

		/**
		 * Get the car range values and add a filter to the query
		 */
		if (!empty($_POST['range_filter']) && is_array($_POST['range_filter'])) {
			$min_range = (int) $_POST['range_filter'][0];
			$max_range = (int) $_POST['range_filter'][1];

			$meta_array[] = [
				'relation' => 'OR',
				[
					'key' => 'range_real_int',
					'value' => [
						$min_range,
						$max_range
					],
					'type' => 'numeric',
					'compare' => 'between'
				],
				[
					'key' => 'range_real_int',
					'value' => '',
					'compare' => '=='
				]
			];
		}

		/**
		 * Get the car range values and add a filter to the query
		 */
		if (!empty($_POST['price_filter']) && is_array($_POST['price_filter'])) {
			$min_range = (int) $_POST['price_filter'][0];
			$max_range = (int) $_POST['price_filter'][1];

			$field_to_filter = 'price';

			$meta_array[] = [
				[
					'relation' => 'AND',
					[
						'key' => $field_to_filter,
						'value' => [
							$min_range,
							$max_range
						],
						'type' => 'numeric',
						'compare' => 'between'
					],
					[
						'key' => $field_to_filter,
						'value' => '',
						'compare' => '!='
					]
				]
			];
		}

		/**
		 * Get the contract filter values and add a filter to the query
		 */
		if (!empty($_POST['contract_filter']) && is_array($_POST['contract_filter'])) {
			$min_range = (int) $_POST['contract_filter'][0];
			$max_range = (int) $_POST['contract_filter'][1];

			$meta_array[] = [
				'key' => 'configurator_duration_int',
				'value' => [
					$min_range,
					$max_range
				],
				'type' => 'numeric',
				'compare' => 'between'
			];
		}

		/**
		 * Get the kilometer filter values and add a filter to the query
		 */
		if (!empty($_POST['km_per_year_filter']) && is_array($_POST['km_per_year_filter'])) {
			$min_range = (int) $_POST['km_per_year_filter'][0];
			$max_range = (int) $_POST['km_per_year_filter'][1];

			$meta_array[] = [
				'key' => 'configurator_kilometers_int',
				'value' => [
					$min_range,
					$max_range
				],
				'type' => 'numeric',
				'compare' => 'between'
			];
		}

		if (!empty($meta_array) && is_array($meta_array)) {
			$meta_query = [
				'relation' => 'AND'
			];

			foreach ($meta_array as $meta_array_item) {
				$meta_query[] = $meta_array_item;
			}
			$args->set_meta_query($meta_query);
		}

		/**
		 * Set order for the custom post type order plugin
		 */

		$order = 'ASC';
		$order_by = 'menu_order';
		$order_choice = esc_html($_POST['order_by']);

		if (!empty($order_choice) && $order_choice !== 'menu_order') {
			$order_choice_exploded = explode('-', $order_choice);
			if (!empty($order_choice_exploded[0])) {
				$order_by = 'meta_value_num';
				$order = !empty($order_choice_exploded[1]) ? $order_choice_exploded[1] : $order;
				$meta_key = $order_choice_exploded[0];
				$args->set_meta_key($meta_key);
			}
		} else {
			$order_by = 'ID';
			$order = 'ASC';
		}

		$args->set_order_by($order_by);
		$args->set_order($order);

		// Get results
		$result = QueryHelper::get_result($args);
		$posts = $result->get_posts();

		// echo '<pre>';
		// var_dump($result->get_count());
		// echo '</pre>';
		// die();

		$all_posts_in_post_type = (int) $_POST['initialCount'];

		$response = [
			'html' => Timber::compile('components/ajax-cars.twig', [
				'posts' => $posts,
				'reviews' => \Brandfirm\Devkit\Reviews::get_reviews(),
			]),
			'count' => $result->get_count() === $per_page ? $all_posts_in_post_type : $result->get_count(),
			// 'transient_key' => $transient_key
		];

		wp_reset_query();
		wp_reset_postdata();

		// set_transient($transient_key, base64_encode(json_encode($response)), 36000);

		wp_send_json($response);
		// }
		wp_die();
	}
}

new FilterCars();
<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the General Public License (GPL 3.0).
 * This license is available through the world-wide-web at this URL:
 * http://opensource.org/licenses/gpl-3.0.en.php
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this module to newer
 * versions in the future.
 *
 * @author      Tommy Kolkman <tommy@steez.nl>
 * @copyright   Copyright (c) 2020 Steez (https://steez.nl)
 * @license     http://opensource.org/licenses/gpl-3.0.en.php General Public License (GPL 3.0)
 *
 */

add_filter('acf/format_value/key=field_5d8a6266c4961', 'format_price_fields', 10, 3);
add_filter('acf/format_value/key=field_5dac328bb7f01', 'format_price_fields', 10, 3);
add_filter('acf/format_value/key=field_5fb65ea0103e5', 'format_price_fields', 10, 3);

function format_price_fields ($field)
{
	
	if (!empty($field)) {
		
		/**
		 * Cast the price to a float.
		 */
		$price_float = (int)$field;
		
		if (0 === $price_float) {
			$price_float = preg_replace("/[^0-9.]/", "", $field);
		}
		
		/**
		 * Setup the formatter
		 */
		$formatter = new NumberFormatter('nl_NL', NumberFormatter::CURRENCY);
		
		$formatter->setTextAttribute(NumberFormatter::CURRENCY_CODE, 'EUR');
		$formatter->setAttribute(NumberFormatter::FRACTION_DIGITS, 0);
		
		//	$formatter = new NumberFormatter( 'nl_NL', NumberFormatter::DECIMAL );
		//	$formatter->setPattern('###.###,###');
		return $formatter->formatCurrency($price_float, 'EUR');
	} else {
		return false;
	}
}

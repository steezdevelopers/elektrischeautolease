<?php

namespace GForms;

use BrandfirmTaxonomies;
use DOMDocument;
use WP_Term;

class GForms
{
	
	/**
	 * GForms constructor.
	 */
	public function __construct ()
	{
		// Filters
		//		add_filter('gform_submit_button', [$this, 'form_submit_button'], 10, 2);
		add_action('gform_pre_submission_5', [$this, 'set_car_model_to_field'], 10, 1);
		add_action('gform_pre_submission_7', [$this, 'set_car_model_to_field'], 10, 1);
		add_filter('gform_pre_render_5', [$this, 'populate_choices'], 10, 1);
		add_filter('gform_pre_render_7', [$this, 'populate_choices'], 10, 1);
		add_filter('gform_pre_render_5', [$this, 'strip_duration_when_occasion'], 20, 1);
		add_filter('gform_submit_button', [$this, 'add_custom_css_classes'], 10, 2);
		
		// Set the lease company
		add_filter('gform_pre_send_email', [$this, 'before_email'], 10, 4);
		add_action('gform_pre_submission', [$this, 'set_lease_company_term'], 10, 1);
	}
	
	/**
	 * Change the emailaddress based on the chosen lease company.
	 * @param $email
	 * @return mixed
	 */
	function before_email ($email, $message_format, $notification, $entry)
	{
		/**
		 * @desc 1. Change the emailaddress based on the chosen lease company.
		 */
		$model_id = 0;
		
		if (!empty($entry['source_url'])) {
			$model_id = $this->get_model_id_from_url($entry['source_url']);
		}
		
		$lease_company_term = $this->get_lease_company_term_by_model_id($model_id);
		
		if (!empty($lease_company_term) && $lease_company_term instanceof WP_Term) {
			// Now check if we can't find the lease company's email
			$lease_company_email = get_field('lease_company_email', sprintf('%s_%s', BrandfirmTaxonomies::LEASE_COMPANY, $lease_company_term->term_id));
			if (!empty($lease_company_email) && is_email($lease_company_email)) {
				$email['headers']['Bcc'] = 'Bcc: ' . get_bloginfo('admin_email');
				$email['subject'] = sprintf('[Verzonden aan %s] %s', $lease_company_term->name, $email['subject']);
				$email['to'] = $lease_company_email;
			}
		}
		
		return $email;
	}
	
	/**
     * Set the lease company term as an entry field.
	 * @param $form
	 * @return void
	 */
	function set_lease_company_term ($form)
	{
		$current_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
		
		$model_id = 0;
		
		if (!empty($current_link)) {
			$model_id = $this->get_model_id_from_url($current_link);
		}
  
		$lease_company_term = $this->get_lease_company_term_by_model_id($model_id);
		
		if (!empty($lease_company_term) && $lease_company_term instanceof WP_Term) {
            // @todo Should we do this hardcoded? :(
            $_POST['input_37'] = $lease_company_term->name;
		}
	}
	
	/**
	 * @param $url
	 * @return int
	 */
	private function get_model_id_from_url ($url)
	{
		$model_id = 0;
		$parsed_url = parse_url($url);
		parse_str($parsed_url['query'], $results);
		if ($results['model_id']) {
			$model_id = $results['model_id'];
		}
		
		return (int)$model_id;
	}
	
	/**
	 * Get the lease company term by providing the model id.
	 * @param $model_id
	 * @return false|WP_Term
	 */
	private function get_lease_company_term_by_model_id ($model_id)
	{
		$lease_company_term = false;
		if (!empty($model_id)) {
			// Check if the model id is added to a term in the taxonomy "lease_company"
			$terms = wp_get_post_terms($model_id, BrandfirmTaxonomies::LEASE_COMPANY);
			
			if (!empty($terms) && is_array($terms)) {
				// Get the first term -- it is up to the client to select only one
				$lease_company_term = $terms[0];
			}
		}
		
		return $lease_company_term;
	}
	
	/**
	 * @param $button
	 * @param $form
	 * @return mixed
	 */
	function add_custom_css_classes ($button, $form)
	{
		$dom = new DOMDocument();
		$dom->loadHTML('<?xml encoding="utf-8" ?>' . $button);
		$input = $dom->getElementsByTagName('input')->item(0);
		$classes = $input->getAttribute('class');
		$classes .= " button__primary button__arrow button__half";
		$input->setAttribute('class', $classes);
		
		return $dom->saveHtml($input);
	}
	
	/**
	 * @param $form
	 */
	function set_car_model_to_field ($form)
	{
		if (!empty($_POST['input_16'])) {
			$_POST['input_16'] = sprintf('%s (%s)', get_field('custom_seo_title', esc_html($_POST['input_16'])), esc_html($_POST['input_16']));
		}
	}
	
	/**
	 * @param $form
	 * @return mixed
	 */
	function populate_choices ($form)
	{
		if (!empty($form['fields'])) {
			foreach ($form['fields'] as $field_key => $field_value) {
				
				if ($field_value->type != 'select' || strpos($field_value->cssClass, 'lease-type') === false) {
					continue;
				}
				
				if (!empty($_GET['type'])) {
					$type_to_select = $_GET['type'];
					$choices = [];
					
					if (!empty($field_value->choices)) {
						foreach ($field_value->choices as $choice_key => $choice_val) {
							$choice = [
								'text' => $choice_val['text'],
								'value' => $choice_val['value'],
								'isSelected' => false
							];
							if (strtolower($choice_val['value']) === $type_to_select) {
								$choice['isSelected'] = true;
								
								?>
                                
                                <script type="text/javascript">
                                    jQuery(document).ready(function () {
                                        /* apply only to a input with a class of gf_readonly */
                                        jQuery('li.lease-type select').attr('disabled', 'disabled');
                                    });
                                </script>
								
								<?php
								
							}
							$choices[] = $choice;
						}
					}
					
					$field_value->choices = $choices;
				}
			}
		}
		
		return $form;
	}
	
	/**
	 * If we're dealing with an occasion, duration is unnecessary.
	 * @param $form
	 * @return mixed
	 */
	public function strip_duration_when_occasion ($form)
	{
		foreach ($form['fields'] as $field_key => $field_value) {
			
			/**
			 * Strip "looptijd" when it is an occasion
			 */
			if (!empty($_GET['occasion']) && 1 == $_GET['occasion']) {
				if ($field_value->id === 24) {
					unset($form['fields'][$field_key]);
				}
			}
		}
		
		return $form;
	}
}

new GForms();

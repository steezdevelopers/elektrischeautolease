<?php
/**
 * maybe_cache.
 *
 * Check if transient cache exist, else set it,
 *
 * @see https://since1979.dev/snippet-008-using-transients-to-cache-data/
 *
 * @uses get_transient() https://developer.wordpress.org/reference/functions/get_transient/
 * @uses is_callable() https://www.php.net/manual/en/function.is-callable.php
 * @uses wp_die() https://developer.wordpress.org/reference/functions/wp_die/
 * @uses call_user_func() https://www.php.net/manual/en/function.call-user-func.php
 * @uses set_transient() https://developer.wordpress.org/reference/functions/set_transient/
 *
 * @param String $cahce The name of the transient
 * @param Int $time The transient timeout
 * @param Callable $callback The callback function
 * @return Mixed
 */
function maybe_cache(String $cache = '', Int $time = 7200, Callable $callback = null)
{
    if ($cachedData = get_transient($cache)){
        return $cachedData;
    }

    if (!$callback || !is_callable($callback))
        wp_die('No (valid) callback function provided.');

    set_transient($cache, $data = call_user_func($callback), $time);

    return $data;
}
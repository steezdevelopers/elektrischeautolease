<?php
/**
 * init
 *
 * @copyright Copyright © 2022 Steez Webdevelopment. All rights reserved.
 * @author    tommy@steez.nl
 */

namespace Brandfirm\Devkit;

use BrandfirmTaxonomies;
use GFFormsModel;

class Init
{
	
	/** @var string */
	const API_URL = 'https://api.mobilityservice.nl/api';
	
	/**
	 * @var string
	 */
	private $access_token;
	
	/**
	 * Mobility Service API call init.
	 */
	public function __construct ()
	{
		$this->access_token = '';
		add_action('gform_after_submission', [$this, 'post_to_mobility_service'], 10, 2);
	}
	
	/**
	 * Post the form data to Mobility Service.
	 * @param $entry
	 * @param $form
	 * @return void
	 */
	public function post_to_mobility_service ($entry, $form)
	{
		if (!empty($form['id']) && 5 === $form['id']) {
			
			$access_token = $this->get_access_token();
			
			$lease_company_entry = rgar($entry, '37');
			$lease_company_term = get_term_by('name', $lease_company_entry, BrandfirmTaxonomies::LEASE_COMPANY);
			
			if (!empty($lease_company_entry) && !empty($lease_company_term) && $lease_company_term->slug === 'mobility-service') {
				$parameters = $this->get_parameters($entry);
				
				if (!empty($parameters)) {
					$output = $this->send_call($access_token, $parameters);
					if ($output['status'] === 'error') {
						GFFormsModel::add_note($entry['id'], 0, __('Mobility Service koppeling'),
							sprintf('ERROR! Entry %s is niet naar Mobility Service gestuurd, door een probleem met de API: %s', $entry['id'], implode(' - ', $output['errors'])),
							true, 'error');
					} else {
						if ($output['status'] === 'success') {
							GFFormsModel::add_note($entry['id'], 0, __('Mobility Service koppeling'),
								sprintf('Entry %s is succesvol naar Mobility Service verstuurd!', $entry['id']), true, 'success');
						}
					}
				}
				
				
			}
		}
	}
	
	/**
	 * @param $access_token
	 * @param $parameters
	 * @return array
	 */
	private function send_call ($access_token, $parameters)
	{
		$output = [];
		
		if (!empty($access_token) && !empty($parameters)) {
			
			$curl = curl_init();
			
			curl_setopt_array($curl, array(
				CURLOPT_URL => sprintf('%s/elektrischeautolease', self::API_URL),
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => '',
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 0,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => 'POST',
				CURLOPT_POSTFIELDS => $parameters,
				CURLOPT_HTTPHEADER => array(
					'Accept: application/json',
					sprintf('Authorization: Bearer %s', $access_token)
				),
			));
			
			$response = curl_exec($curl);
			
			curl_close($curl);
			
			if (!empty($response)) {
				$json_object = json_decode($response);
				if (!empty($json_object) && is_object($json_object) && !empty($json_object->status)) {
					$output['status'] = $json_object->status;
					if (!empty($json_object->errors)) {
						$output['errors'] = $json_object->errors;
					}
				}
			}
		}
		
		return $output;
	}
	
	/**
	 * Get the necessary parameters for our API call to the Mobility Service API.
	 * @param $entry
	 * @return array
	 */
	private function get_parameters ($entry)
	{
		/**
		 * {
		 * "companyname":"Jolt van der Veen BV",
		 * "firstname":"Jolt",
		 * "lastname":"van der Veen",
		 * "email":"jolt@mobilityservice.nl",
		 * "phone":"0610617942",
		 * "car":"Audi e-tron Sportback 55 quattro (3650)",
		 * "terms":"60",
		 * "kilometres":"10000",
		 * "winterTires":"{waarde van je opties}",
		 * "chargingAndCard":"{waarde van je opties}",
		 * "replacementVehicle":"{waarde van je opties}",
		 * }
		 */
		
		$parameters = [];
		
		if (!empty($entry)) {
			$parameters['companyname'] = esc_html(rgar($entry, '22'));
			$parameters['firstname'] = esc_html(rgar($entry, '18'));
			$parameters['lastname'] = esc_html(rgar($entry, '19'));
			$parameters['email'] = esc_html(rgar($entry, '11'));
			$parameters['phone'] = esc_html(rgar($entry, '21'));
			$parameters['car'] = esc_html(rgar($entry, '16'));
			$parameters['terms'] = esc_html(rgar($entry, '24'));
			$parameters['kilometres'] = esc_html(rgar($entry, '23'));
			$parameters['winterTires'] = esc_html(rgar($entry, '31'));
			$parameters['chargingAndCard'] = esc_html(rgar($entry, '29'));
			$parameters['replacementVehicle'] = esc_html(rgar($entry, '34'));
			$parameters['comments'] = esc_html(rgar($entry, '12'));
		}
		
		return $parameters;
	}
	
	/**
	 * Check if the access token is set. If not, get it from Mobility Service.
	 * @return string
	 */
	private function get_access_token ()
	{
		if (empty($this->access_token)) {
			$curl = curl_init();
			
			curl_setopt_array($curl, array(
				CURLOPT_URL => sprintf('%s/login', self::API_URL),
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => '',
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 0,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => 'POST',
				CURLOPT_POSTFIELDS => [
					'email' => MOBILITY_SERVICE_USERNAME,
					'password' => MOBILITY_SERVICE_PASSWORD
				],
				CURLOPT_HTTPHEADER => array(
					'Accept: application/json'
				),
			));
			
			$response = curl_exec($curl);
			
			curl_close($curl);
			
			if (!empty($response)) {
				$json_object = json_decode($response);
				if (!empty($json_object) && is_object($json_object) && !empty($json_object->access_token)) {
					$this->access_token = $json_object->access_token;
				}
			}
		}
		
		return $this->access_token;
	}
}

new Init();

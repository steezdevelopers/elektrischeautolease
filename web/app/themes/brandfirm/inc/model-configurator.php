<?php
/**
 * ModelConfigurator
 *
 * @copyright Copyright © 2021 Steez Webdevelopment. All rights reserved.
 * @author    tommy@steez.nl
 */

namespace Brandfirm\Devkit;


class ModelConfigurator
{
	
	/**
	 * @var false|int
	 */
	private $model_id;
	
	/**
	 * ModelConfigurator constructor.
	 */
	public function __construct ()
	{
		$this->model_id = get_the_ID();
	}
	
	/**
	 * @return false[]
	 */
	public function get_prices ()
	{
		$prices = [
			'business' => false,
			'private' => false
		];
		
		if (!empty(get_field('price', $this->model_id))) {
			$prices['business'] = get_field('price', $this->model_id);
		}
		
		if (!empty(get_field('price_private', $this->model_id))) {
			$prices['private'] = get_field('price_private', $this->model_id);
		}
		
		return $prices;
	}
	
	/**
	 * @return mixed|string[]
	 */
	public function get_configuration_duration ()
	{
		$duration = [
			'12',
			'24',
			'36',
			'48',
			'60'
		];
		
		if (!empty(get_field('configurator_duration', $this->model_id))) {
			$duration = get_field('configurator_duration', $this->model_id);
		}
		
		return $duration;
	}
	
	/**
	 * @return mixed|string[]
	 */
	public function get_configuration_kilometers ()
	{
		$duration = [
			[
				'label' => '10.000km',
				'value' => '10000'
			],
			[
				'label' => '15.000km',
				'value' => '15000'
			],
			[
				'label' => '20.000km',
				'value' => '20000'
			],
			[
				'label' => '25.000km',
				'value' => '25000'
			],
			[
				'label' => '30.000km',
				'value' => '30000'
			],
			[
				'label' => '35.000km',
				'value' => '35000'
			],
			[
				'label' => '40.000km',
				'value' => '40000'
			],
			[
				'label' => '45.000km',
				'value' => '45000'
			],
			[
				'label' => '50.000km',
				'value' => '50000'
			],
			[
				'label' => '55.000km',
				'value' => '55000'
			],
			[
				'label' => '60.000km',
				'value' => '60000'
			],
			[
				'label' => '60.000km +',
				'value' => '65000'
			]
		];
		
		if (!empty(get_field('configurator_kilometers', $this->model_id))) {
			$duration = get_field('configurator_kilometers', $this->model_id);
		}
		
		return $duration;
	}
	
	/**
	 * @return mixed
	 */
	public function get_quote_page_link_business ()
	{
		$quote_page = '';
		if (!empty(get_field('quote_page_business', 'options'))) {
			$quote_page = get_field('quote_page_business', 'options');
		}
		
		return $quote_page;
	}
	
	/**
	 * @return mixed
	 */
	public function get_quote_page_link_private ()
	{
		$quote_page = '';
		if (!empty(get_field('quote_page_private', 'options'))) {
			$quote_page = get_field('quote_page_private', 'options');
		}
		
		return $quote_page;
	}
}

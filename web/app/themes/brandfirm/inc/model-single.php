<?php

use Brandfirm\Devkit\ModelConfigurator;
use Timber\Timber;

/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the General Public License (GPL 3.0).
 * This license is available through the world-wide-web at this URL:
 * http://opensource.org/licenses/gpl-3.0.en.php
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this module to newer
 * versions in the future.
 *
 * @author      Tommy Kolkman <tommy@steez.nl>
 * @copyright   Copyright (c) 2020 Steez (https://steez.nl)
 * @license     http://opensource.org/licenses/gpl-3.0.en.php General Public License (GPL 3.0)
 *
 */
class ModelSingle
{
	
	/** @var string */
	const POST_TYPE = 'models';
	
	/** @var int */
	const STOCK_CAR_TERM = 187;
	
	/**
	 * ModelSingle constructor.
	 */
	public function __construct ()
	{
		add_filter('timber/context', [$this, 'init_model_single_construction'], 10, 1);
		add_filter('wpseo_breadcrumb_links', [$this, 'wpseo_remove_home_breadcrumb'], 10, 1);
		add_filter('wpseo_breadcrumb_links', [$this, 'wpseo_split_brand_and_model_links'], 20, 1);
	}
	
	/**
	 * Add model landingpages to the breadcrumbs.
	 * @param $links
	 * @return mixed
	 */
	public function wpseo_split_brand_and_model_links ($links)
	{
		if (!empty($links[3])) {
			$model_id = $links[3]['id'];
			
			if ($model_name = get_field('model_name') && $model_version = get_field('model_version')) {
				
				$terms = wp_get_post_terms($model_id, 'modellandingspage');
				
				if (!empty($terms) && !empty($terms[0])) {
					$link = get_term_link($terms[0]);
					$links[3] = [
						'url' => $link,
						'text' => get_field('model_name'),
						'term_id' => ''
					];
					$links[4] = [
						'url' => '',
						'text' => $model_version,
						'id' => $model_id
					];
				}
			}
		}
		
		return $links;
	}
	
	/**
	 * Remove the home link from the breadcrumbs.
	 * @param $links
	 * @return mixed
	 */
	public function wpseo_remove_home_breadcrumb ($links)
	{
		if (!is_front_page()) {
			if (!empty($links[1]['ptarchive'])) {
				if ($links[1]['ptarchive'] == 'models' && is_singular('models')) {
					unset($links[1]);
				}
			}
		}
		
		return $links;
	}
	
	/**
	 * Add the deals table to the single model page.
	 * @param $context
	 * @return mixed
	 */
	public function init_model_single_construction ($context)
	{
		$model_id = get_the_ID();
		
		if (!empty($model_id)) {
			$context['single_model']['model_information'] = $this->get_model_basic_information();
			$context['single_model']['gallery_images'] = $this->get_model_gallery_images('large');
			$context['single_model']['gallery_thumbnails'] = $this->get_model_gallery_images('gallery_thumbnail');
			$context['single_model']['detail_tables'] = $this->get_model_detail_tables();
			$context['single_model']['results_text'] = $this->get_results_text();
			$context['single_model']['usps_general'] = $this->get_usps_general();
			$context['single_model']['top_5_models'] = $this->get_top_5_models();
			$context['single_model']['charging_electrical_car_image'] = get_template_directory_uri() . '/dist/images/charging_electric_car.jpg';
			
			// Initialize configurator
			$configurator = new ModelConfigurator();
			
			$context['single_model']['prices'] = $configurator->get_prices();
			$context['single_model']['configuration']['duration'] = $configurator->get_configuration_duration();
			$context['single_model']['configuration']['kilometers'] = $configurator->get_configuration_kilometers();
			$context['single_model']['configuration']['quote_page']['business'] = $configurator->get_quote_page_link_business();
			$context['single_model']['configuration']['quote_page']['private'] = $configurator->get_quote_page_link_private();
			$context['single_model']['quote_button_text'] = $this->get_quote_button_text();
			$context['single_model']['all_models_text'] = 'Bekijk alle modellen';
			$context['single_model']['all_models_link'] = get_post_type_archive_link('models');
			
			// Is stock car
			$context['single_model']['is_stock_car'] = $this->is_stock_car();
			
		}
		
		return $context;
	}
	
	/**
	 * @return bool
	 */
	private function is_stock_car ()
	{
		$model_id = get_the_ID();
		
		return has_term(self::STOCK_CAR_TERM, 'lease', $model_id);
	}
	
	/**
	 * @return array
	 */
	private function get_model_basic_information (): array
	{
		$model_id = get_the_ID();
		$model_information = [];
		$model_information['id'] = $model_id;
		
		$model_information['title'] = get_the_title();
		if (!empty(get_field('custom_seo_title', $model_id))) {
			$model_information['title'] = get_field('custom_seo_title', $model_id);
		}
		
		if (!empty(get_field('subtitle', $model_id))) {
			$model_information['subtitle'] = get_field('subtitle', $model_id);
		}
		
		if (!empty(get_field('description', $model_id))) {
			$model_information['description'] = get_field('description', $model_id);
		}
		
		if (!empty(get_field('occasion_column_1', $model_id))) {
			$model_information['occasion_column_1'] = wpautop(get_field('occasion_column_1', $model_id));
		}
		
		if (!empty(get_field('occasion_column_2', $model_id))) {
			$model_information['occasion_column_2'] = wpautop(get_field('occasion_column_2', $model_id));
		}
		
		if (!empty(get_field('reference', $model_id))) {
			$model_information['reference'] = get_field('reference', $model_id);
		}
		
		return $model_information;
	}
	
	/**
	 * @desc Get the right images for the gallery on single models.
	 *
	 * @param $key
	 *
	 * @return array
	 */
	private function get_model_gallery_images ($key)
	{
		$gallery_images = [];
		
		if (is_single() && 'models' === get_post_type()) {
			
			// Get the images from ACF
			$carousel_images = get_field('carousel_images', get_the_ID());
			
			if (!empty($carousel_images)) {
				/** @var array $carousel_image */
				foreach ($carousel_images as $carousel_object) {
					if (!empty($carousel_object['image']['sizes'])) {
						$sizes = $carousel_object['image']['sizes'];
						$gallery_images[] = [
							'src' => $sizes[$key],
							'alt' => $carousel_object['image']['name']
						];
					}
				}
			}
			
		}
		
		return $gallery_images;
	}
	
	/**
	 * @return array
	 */
	private function get_model_detail_tables ()
	{
		$model_id = get_the_ID();
		
		$model_title = get_the_title();
		if (!empty(get_field('custom_seo_title', $model_id))) {
			$model_title = get_field('custom_seo_title', $model_id);
		}
		
		/**
		 * General information
		 */
		$table['general_information']['title'] = sprintf('Algemene informatie %s', $model_title);
		$table['general_information']['description'] = get_field('description', $model_id);
		
		if (function_exists('format_price_fields')) {
			$table['general_information']['table'][] = [
				'value' => format_price_fields(get_field('price_from_nl', $model_id)),
				'label' => 'Vanafprijs'
			];
		} else {
			$table['general_information']['table'][] = [
				'value' => get_field('price_from_nl', $model_id),
				'label' => 'Vanafprijs'
			];
		}
		
		if (function_exists('format_price_fields')) {
			$table['general_information']['table'][] = [
				'value' => format_price_fields(get_field('price_from_nl_fiscal', $model_id)),
				'label' => 'Fiscale waarde auto'
			];
		} else {
			$table['general_information']['table'][] = [
				'value' => get_field('price_from_nl_fiscal', $model_id),
				'label' => 'Fiscale waarde auto'
			];
		}
		$table['general_information']['table'][] = [
			'value' => get_field('range_wltp', $model_id),
			'label' => 'Actieradius opgegeven door fabrikant'
		];
		$table['general_information']['table'][] = [
			'value' => get_field('range_real', $model_id),
			'label' => 'Actieradius in de praktijk'
		];
		$table['general_information']['table'][] = [
			'value' => get_field('fastcharge_chargetime', $model_id),
			'label' => 'Minuten nodig om tot 80% te laden'
		];
		$table['general_information']['table'][] = [
			'value' => get_field('performance_acceleration', $model_id),
			'label' => 'Acceleratie van 0-100 km/uur'
		];
		
		/**
		 * Load information
		 */
		$table['load_information']['title'] = 'Informatie over het laden';
		$table['load_information']['description'] = get_field('description_charging', $model_id);
		$table['load_information']['table'][] = [
			'value' => get_field('charge_plug', $model_id),
			'label' => 'Soort aansluiting'
		];
		$table['load_information']['table'][] = [
			'value' => get_field('fastcharge_plug', $model_id),
			'label' => 'Type snellaadpoort'
		];
		$table['load_information']['table'][] = [
			'value' => get_field('fastcharge_chargetime', $model_id),
			'label' => 'Minuten nodig om tot 80% te laden'
		];
		$table['load_information']['table'][] = [
			'value' => get_field('charge_standard_chargetime', $model_id),
			'label' => 'Minuten nodig om tot 100% te laden'
		];
		$table['load_information']['table'][] = [
			'value' => get_field('fastcharge_power_max', $model_id),
			'label' => 'Maximale power bij snelladen'
		];
		$table['load_information']['table'][] = [
			'value' => get_field('charge_standard_power', $model_id),
			'label' => 'Maximale power bij standaard laden'
		];
		$table['load_information']['table'][] = [
			'value' => get_field('fastcharge_chargeSpeed', $model_id),
			'label' => 'Snellaadtijd in km/uur'
		];
		$table['load_information']['table'][] = [
			'value' => get_field('range_wltp', $model_id),
			'label' => 'Actieradius opgegeven door fabrikant'
		];
		$table['load_information']['table'][] = [
			'value' => get_field('range_real', $model_id),
			'label' => 'Actieradius in de praktijk'
		];
		$table['load_information']['table'][] = [
			'value' => get_field('range_real_bcity', $model_id),
			'label' => 'Actieradius in de stad'
		];
		$table['load_information']['table'][] = [
			'value' => get_field('range_real_bhwy', $model_id),
			'label' => 'Actieradius op de snelweg'
		];
		
		/**
		 * Technical specifications
		 */
		$table['tech_specs']['title'] = sprintf('Technische specificaties van de %s', $model_title);
		$table['tech_specs']['description'] = get_field('description_technical', $model_id);
		$table['tech_specs']['table'][] = [
			'value' => get_field('performance_topspeed', $model_id),
			'label' => 'Topsnelheid'
		];
		$table['tech_specs']['table'][] = [
			'value' => get_field('drivetrain_power', $model_id),
			'label' => 'Totaal vermogen'
		];
		$table['tech_specs']['table'][] = [
			'value' => get_field('drivetrain_torque', $model_id),
			'label' => 'Koppel (NM)'
		];
		$table['tech_specs']['table'][] = [
			'value' => get_field('misc_seats', $model_id),
			'label' => 'Aantal zitplaatsen'
		];
		$table['tech_specs']['table'][] = [
			'value' => get_field('dims_weight', $model_id),
			'label' => 'Gewicht van de auto'
		];
		$table['tech_specs']['table'][] = [
			'value' => get_field('drivetrain_propulsion', $model_id),
			'label' => 'Soort aandrijving'
		];
		$table['tech_specs']['table'][] = [
			'value' => get_field('dims_length', $model_id),
			'label' => 'Lengte van de auto'
		];
		$table['tech_specs']['table'][] = [
			'value' => get_field('dims_width', $model_id),
			'label' => 'Breedte van de auto'
		];
		$table['tech_specs']['table'][] = [
			'value' => get_field('dims_height', $model_id),
			'label' => 'Hoogte van de auto'
		];
		$table['tech_specs']['table'][] = [
			'value' => get_field('dims_wheelbase', $model_id),
			'label' => 'Wielbasis van de auto'
		];
		
		/**
		 * Stock car information
		 */
		$table['stock_car_information']['title'] = sprintf('Eigenschappen %s', $model_title);
		$table['stock_car_information']['description'] = get_field('description', $model_id);
		$table['stock_car_information']['table'][] = [
			'value' => get_field('fuel', $model_id),
			'label' => 'Brandstof'
		];
		$table['stock_car_information']['table'][] = [
			'value' => get_field('transmission', $model_id),
			'label' => 'Transmissie'
		];
		$table['stock_car_information']['table'][] = [
			'value' => get_field('range_real', $model_id),
			'label' => 'Actieradius in de praktijk'
		];
		$table['stock_car_information']['table'][] = [
			'value' => get_field('misc_seats', $model_id),
			'label' => 'Aantal zitplaatsen'
		];
		$table['stock_car_information']['table'][] = [
			'value' => get_field('doors', $model_id),
			'label' => 'Aantal deuren'
		];
		$table['stock_car_information']['table'][] = [
			'value' => get_field('filter_color', $model_id),
			'label' => 'Kleur'
		];
		$types = wp_get_post_terms($model_id, 'type', ['fields' => 'names']);
		$table['stock_car_information']['table'][] = [
			'value' => !empty($types[0]) ? $types[0] : '',
			'label' => 'Carrosserie'
		];
		$table['stock_car_information']['table'][] = [
			'value' => get_field('reference', $model_id),
			'label' => 'Referentienummer'
		];
		$brands = wp_get_post_terms($model_id, 'brand', ['fields' => 'names']);
		$table['stock_car_information']['table'][] = [
			'value' => !empty($brands[0]) ? $brands[0] : '',
			'label' => 'Merk'
		];
		$table['stock_car_information']['table'][] = [
			'value' => get_field('model_name', $model_id),
			'label' => 'Model'
		];
		$table['stock_car_information']['table'][] = [
			'value' => get_field('model_version', $model_id),
			'label' => 'Uitvoering'
		];
		$lease_type = wp_get_post_terms($model_id, 'lease', ['fields' => 'names']);
		$table['stock_car_information']['table'][] = [
			'value' => !empty($lease_type[0]) ? $lease_type[0] : '',
			'label' => 'Staat voertuig'
		];
		$table['stock_car_information']['table'][] = [
			'value' => get_field('year_of_production', $model_id),
			'label' => 'Bouwjaar'
		];
		$table['stock_car_information']['table'][] = [
			'value' => get_field('tax_addition', $model_id) . '%',
			'label' => 'Bijtelling'
		];
		
		return $table;
	}
	
	/**
	 * @return array
	 */
	private function get_results_text ()
	{
		$results_text = [];
		$model_id = get_the_ID();
		
		$model_title = get_the_title();
		if (!empty(get_field('custom_seo_title', $model_id))) {
			$model_title = get_field('custom_seo_title', $model_id);
		}
		
		if (!empty($model_id)) {
			$results_text['title'] = sprintf('Prestaties van de %s', $model_title);
			$results_text['description'] = get_field('description_general', $model_id);
		}
		
		return $results_text;
	}
	
	/**
	 * @return mixed
	 */
	private function get_usps_general ()
	{
		$model_id = get_the_ID();
		
		$usps_general = [];
		
		if (!empty(get_field('usps_general', $model_id))) {
			$usps_general = get_field('usps_general', $model_id);
		} else {
			$usps_general = get_field('usps_general', 'options');
		}
		
		return $usps_general;
	}
	
	/**
	 * @return mixed
	 */
	private function get_top_5_models ()
	{
		$top_5_models = [];
		
		$key = 'top_5_models';
		
		if (is_single() && get_post_type() === 'models' && has_term(self::STOCK_CAR_TERM, 'lease', get_queried_object_id())) {
			$key = 'top_5_models_stock';
		}
		
		if (!empty(get_field($key, 'options'))) {
			$top_5_models_acf = get_field($key, 'options');
			if (!empty($top_5_models_acf)) {
				foreach ($top_5_models_acf as $top_5_model_acf) {
					$top_5_models[] = Timber::get_post($top_5_model_acf);
				}
			}
		}
		
		return $top_5_models;
	}
	
	/**
	 * @return mixed
	 */
	private function get_quote_button_text ()
	{
		return !empty(get_field('quote_button_text', 'options')) ? get_field('quote_button_text', 'options') : 'Offerte aanvragen';
	}
	
}

new ModelSingle();

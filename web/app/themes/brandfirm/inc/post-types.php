<?php

/** Post types @package Brandfirm */
class BrandfirmPostTypes
{
	
	const POST = 'post'; // Default
	const PAGE = 'page';
	const FAQ = 'faq';
	const VACANCIES = 'vacansies';
	const MODELS = 'models';
	const MODEL_COLLECTIONS = 'model_collections';
	const USPS = 'usps';
	const BENEFITS = 'benefits';
	const LEASE_COMPANY = 'lease_company';
	const LEASE_COMPANY_DEAL = 'lease_company_deal';
	const CTA = 'cta';
	
	function register_all ()
	{
		$this->register_pages();
		$this->register_post();
		$this->register_faq();
		$this->register_usps();
		$this->register_models();
		$this->register_model_collections();
		$this->register_benefits();
		$this->register_cta();
		$this->register_reviews();
	}
	
	function register_pages ()
	{
	}
	
	function register_post ()
	{
	}
	
	function register_faq ()
	{
		$post_type = 'faq';
		$args = $this->get_default_args(
			$post_type,
			'FAQ',
			'FAQ',
			'FAQ',
			'faq'
		);
		
		$args = array_merge($args, [
			'menu_icon' => 'dashicons-format-chat',
			'exclude_from_search' => true
		]);
		
		register_post_type($post_type, $args);
		$this->add_capabilities_to_admin_role($post_type);
	}
	
	private function get_default_args ($post_type, $name, $singular_name, $menu_name, $slug)
	{
		$labels = [
			'name' => _x($name, 'post-type general name'),
			'singular_name' => _x($singular_name, 'post-type singular name'),
			'menu_name' => _x($menu_name, 'post-type menu name'),
			'name_admin_bar' => __($singular_name),
			'all_items' => __($name),
			'add_new' => __('Add New'),
			'add_new_item' => __(sprintf('Add New %s', $singular_name)),
			'edit_item' => __(sprintf('Edit %s', $singular_name)),
			'new_item' => __(sprintf('New %s', $singular_name)),
			'view_item' => __(sprintf('View %s', $singular_name)),
			'search_items' => __(sprintf('Search %s', $name)),
			'not_found' => __(sprintf('No %s found', strtolower($singular_name))),
			'not_found_in_trash' => __(sprintf('No %s found in Trash', strtolower($singular_name))),
			'parent_item_colon' => __('Parent Item:'),
		];
		
		return [
			'labels' => $labels,
			'public' => true,
			'exclude_from_search' => false,
			'publicly_queryable' => true,
			'show_ui' => true,
			'show_in_nav_menus' => true,
			'show_in_menu' => true,
			'show_in_admin_bar' => true,
			'menu_position' => 21,
			'menu_icon' => 'dashicons-admin-post',
			'capability_type' => $post_type,
			'map_meta_cap' => true,
			'hierarchical' => false,
			'supports' => ['title', 'editor', 'excerpt', 'thumbnail', 'author'],
			'taxonomies' => [],
			'has_archive' => false,
			'rewrite' => ['slug' => $slug, 'with_front' => false, 'pages' => false],
			'query_var' => true,
			'can_export' => true,
		];
	}
	
	function add_capabilities_to_admin_role ($post_type)
	{
		// Add new capabilities to administrator role for managing this type
		$administrator = get_role('administrator');
		$administrator->add_cap(sprintf('edit_%s', $post_type));
		$administrator->add_cap(sprintf('read_%s', $post_type));
		$administrator->add_cap(sprintf('delete_%s', $post_type));
		$administrator->add_cap(sprintf('edit_%ss', $post_type));
		$administrator->add_cap(sprintf('edit_others_%ss', $post_type));
		$administrator->add_cap(sprintf('publish_%ss', $post_type));
		$administrator->add_cap(sprintf('read_private_%ss', $post_type));
		$administrator->add_cap(sprintf('delete_%ss', $post_type));
		$administrator->add_cap(sprintf('delete_private_%ss', $post_type));
		$administrator->add_cap(sprintf('delete_published_%ss', $post_type));
		$administrator->add_cap(sprintf('delete_others_%ss', $post_type));
		$administrator->add_cap(sprintf('edit_private_%ss', $post_type));
		$administrator->add_cap(sprintf('edit_published_%ss', $post_type));
	}
	
	function register_usps ()
	{
		$post_type = 'usps';
		$args = $this->get_default_args(
			$post_type,
			'USPS',
			'USP',
			'USPS',
			'usp'
		);
		
		$args = array_merge($args, [
			'menu_icon' => 'dashicons-saved',
			'exclude_from_search' => true
		]);
		
		register_post_type($post_type, $args);
		$this->add_capabilities_to_admin_role($post_type);
	}
	
	function register_models ()
	{
		$post_type = self::MODELS;
		$args = $this->get_default_args(
			$post_type,
			'Modellen',
			'Model',
			'Modellen',
			'elektrische-autos'
		);
		
		$args = array_merge($args, [
			'taxonomies' => [
				'actieradius',
				'doors',
				'type',
				'lease',
				'seats',
				'brand',
				'seats',
				BrandfirmTaxonomies::MODELLANDINGSPAGE,
				BrandfirmTaxonomies::AVAILABILITY,
				BrandfirmTaxonomies::DELIVERY_TIMES,
				BrandfirmTaxonomies::TAX_ADDITION,
				BrandfirmTaxonomies::LEASE_COMPANY
			],
			'menu_icon' => 'dashicons-car',
			'rewrite' => 'model',
			'has_archive' => 'elektrische-autos',
			'supports' => ['title', 'excerpt', 'thumbnail', 'author'],
		]);
		
		register_post_type($post_type, $args);
		$this->add_capabilities_to_admin_role($post_type);
	}
	
	function register_model_collections ()
	{
		$post_type = self::MODEL_COLLECTIONS;
		$args = $this->get_default_args(
			$post_type,
			'Modelcollecties',
			'Modelcollectie',
			'Modelcollecties',
			'elektrische-auto-modellen'
		);
		
		$args = array_merge($args, [
			'taxonomies' => ['brand'],
			'menu_icon' => 'dashicons-excerpt-view',
			'has_archive' => false
		]);
		
		register_post_type($post_type, $args);
		$this->add_capabilities_to_admin_role($post_type);
	}
	
	// Helper functions
	
	function register_benefits ()
	{
		$post_type = self::BENEFITS;
		$args = $this->get_default_args(
			$post_type,
			'Benefits',
			'Benefit',
			'Benefits',
			'benefit'
		);
		
		$args = array_merge($args, [
			'menu_icon' => 'dashicons-editor-ol',
			'exclude_from_search' => true
		]);
		
		register_post_type($post_type, $args);
		$this->add_capabilities_to_admin_role($post_type);
	}
	
	function register_cta ()
	{
		$post_type = self::CTA;
		$args = $this->get_default_args(
			$post_type,
			'CTA',
			'cta',
			'CTA',
			'cta'
		);
		
		$args = array_merge($args, [
			'menu_icon' => 'dashicons-feedback',
			'exclude_from_search' => true,
			'supports' => ['title']
		]);
		
		register_post_type($post_type, $args);
		$this->add_capabilities_to_admin_role($post_type);
	}
	
	function register_reviews ()
	{
		$post_type = 'review';
		$args = $this->get_default_args(
			$post_type,
			'Review',
			'Review',
			'Reviews',
			'review'
		);
		
		$args = array_merge($args, [
			'menu_icon' => 'dashicons-clipboard',
			'exclude_from_search' => true,
			'supports' => ['title']
		]);
		
		register_post_type($post_type, $args);
		$this->add_capabilities_to_admin_role($post_type);
	}
}

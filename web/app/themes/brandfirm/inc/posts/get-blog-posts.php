<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the General Public License (GPL 3.0).
 * This license is available through the world-wide-web at this URL:
 * http://opensource.org/licenses/gpl-3.0.en.php
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this module to newer
 * versions in the future.
 *
 * @author      Tommy Kolkman <tommy@steez.nl>
 * @copyright   Copyright (c) 2020 Steez (https://steez.nl)
 * @license     http://opensource.org/licenses/gpl-3.0.en.php General Public License (GPL 3.0)
 *
 */

/**
 * @return array
 */
function set_blogs_to_context ()
{
	$context = Timber\Timber::get_context();
	if (get_post_type() === 'post') {
		
		$args = [
			'post_type' => 'post',
			'posts_per_page' => -1
		];
		
		if (is_category()) {
			$args['cat'] = get_queried_object_id();
		}
		
		$wp_posts = Timber\Timber::get_posts($args);
		
		if (!empty($wp_posts)) {
			$count = 1;
			foreach ($wp_posts as $post) {
				if ($count <= 4) {
					/**
					 * Add it to our custom post loop.
					 */
					$context['featured_posts'][$post->ID] = [
						'model' => $post,
						'excerpt' => get_the_excerpt($post->ID)
					];
				} else {
					$context['normal_posts'][$post->ID] = [
						'model' => $post,
						'excerpt' => get_the_excerpt($post->ID)
					];
				}
				$count++;
			}
		}
	}
	
	return $context;
}

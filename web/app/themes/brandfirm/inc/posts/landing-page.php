<?php

use Timber\Image;
use Timber\Post;
use Timber\Term;

/** Landing Page @package brandfirm */
class LandingPage
	extends AbstractPost
{
	
	private $sections = false;
	
	function sections ()
	{
		if ($this->sections === false) {
			$this->sections = SectionHelper::decorate(
				$this->get_field('bf_sections')
			);
		}
		
		return $this->sections;
	}
}

class SectionHelper
{
	
	static function decorate ($sections)
	{
		if (empty($sections)) {
			return array();
		}
		
		foreach ($sections as $key => $section) {
			switch ($section['acf_fc_layout']) {
				
				case 'blog':
					$sections[$key] = self::decorate_blog($section);
					break;
				
				case 'columns':
					$sections[$key] = self::decorate_columns($section);
					break;
				
				case 'form':
					$sections[$key] = self::decorate_form($section);
					break;
				
				case 'models':
					$sections[$key] = self::decorate_news($section);
					break;
				
				default: // No decoration needed for other sections
					break;
			}
		}
		
		return $sections;
	}
	
	
	static function decorate_columns ($section)
	{
		$section['left'] = self::decorate_column($section['columns']['left']);
		$section['right'] = self::decorate_column($section['columns']['right']);
		
		return $section;
	}
	
	static function decorate_column ($column)
	{
		if ($column['type'] === 'image') {
			$column['image'] = (!empty($column['image']) ? new Timber\Image($column['image']) : null);
		}
		
		if ($column['type'] == 'form') {
			
			if (class_exists('GFCommon')) { // If GF plugin is activated
				$form_html = gravity_form(
					$column['form'],
					true,
					true,
					false,
					null,
					false,
					1,
					false
				);
				
				$column['form'] = $form_html;
			} else {
				$column['form'] = 'No GF';
			}
		}
		
		return $column;
	}
	
	static function decorate_form ($section)
	{
		if (class_exists('GFCommon')) { // If GF plugin is activated
			$form_html = gravity_form(
				$column['form'],
				true,
				true,
				false,
				null,
				false,
				1,
				false
			);
			
			$section['form'] = $form_html;
		} else {
			$section['form'] = 'No GF';
		}
		
		return $section;
	}
	
	static function decorate_news ($section)
	{
		$args = array(
			'post_type' => 'post',
			'posts_per_page' => 3,
			'orderby' => array(
				'date' => 'DESC'
			)
		);
		
		$section['news'] = Timber::get_posts($args);
		
		return $section;
	}
	
	
	static function decorate_models ($section)
	{
		$types = 'models';
		$per_page = $section['limit'] ? intval($section['limit']) : 8;
		$paged = get_query_var('paged', 1);
		$args = new QueryArguments($types, $per_page, $paged);
		
		// Get results
		$result = QueryHelper::get_result($args);
		$section['models'] = $result->get_posts();
		$section['brand'] = Timber::get_terms('brand');
		$section['seats'] = Timber::get_terms('seats');
		$section['labels'] = Timber::get_terms('label');
		
		
		// Check to show pagination
		if ($section['show_pagination'] == true) {
			// Only show pagination if needed
			if (!($paged === 1 && count($result->get_posts()) < $per_page)) {
				$section['pagination'] = $result->get_pagination();
			}
		}
		
		return $section;
	}
	
	static function get_model_collection_list ()
	{
		// 1. Alle terms ophalen
		// 2. Dan alle auto's per term ophalen
		// 3. Dan ook nog alle modelcollections in die term ophalen
		$model_collection_list = [];
		
		$brands = Timber::get_terms('brand');
		
		if (!empty($brands)) {
			/** @var Term $brand */
			foreach ($brands as $brand) {
				if (!empty($brand->ID)) {
					$model_collection_list[$brand->ID]['brand'] = new Term($brand);
					$model_collection_list[$brand->ID]['image'] = self::get_brand_image($brand->ID);
					$model_collection_list[$brand->ID]['models'] = self::get_models_by_brand_id($brand->ID);
				}
			}
		}
		
		return $model_collection_list;
	}
	
	static function get_brand_image ($brand_id)
	{
		$image = false;
		
		if (!empty($brand_id)) {
			$field_key = 'brand_custom_image';
			$field_post_id = 'brand_' . $brand_id;
			$image_id = get_field($field_key, $field_post_id);
			
			if (!empty($image_id)) {
				$image = new Image($image_id);
			}
			
		}
		
		return $image;
	}
	
	static function get_models_by_brand_id ($brand_id)
	{
		$models = [];
		
		$taxonomies_to_exclude = [];
		
		$queried_object = get_queried_object();
		if (!empty($queried_object) && $queried_object instanceof WP_Post) {
			if (have_rows('bf_sections')) {
				while (have_rows('bf_sections')) {
					the_row();
					if (get_row_layout() == 'brands') {
						$taxonomies_to_exclude = get_sub_field('brands_exclude_leases');
					}
				}
			}
		}
		
		if (!empty($brand_id)) {
			
			// Get all cars for the given brand ids
			$args = array(
				'post_type' => ['models', 'model_collections'],
				'tax_query' => array(
					array(
						'taxonomy' => 'brand',
						'field' => 'term_id',
						'terms' => [$brand_id],
						'operator' => 'IN',
					),
				)
			);
			
			if (!empty($taxonomies_to_exclude)) {
				$args['relation'] = 'AND';
				$args['tax_query'][] = [
					'taxonomy' => 'lease',
					'field' => 'term_id',
					'terms' => $taxonomies_to_exclude,
					'operator' => 'NOT IN',
				];
			}
			
			/** @var WP_Query $query */
			$query = new WP_Query($args);
			
			if ($query->have_posts()) {
				while ($query->have_posts()) {
					global $post;
					$query->the_post();
					
					if (!get_field('exclude_from_archive', get_the_ID())) {
						$post_to_add = new Post($post);
						$post_to_add->update('custom_model_collection_link', false);
						
						if ($model_collection_link_taxonomy = get_field('model_collection_link_taxonomy')) {
							try {
								$term_link = get_term_link($model_collection_link_taxonomy, BrandfirmTaxonomies::MODELLANDINGSPAGE);
								$post_to_add->update('custom_model_collection_link', $term_link);
							} catch (Exception $e) {
							}
						}
						
						$models[] = $post_to_add;
					}
				}
			}
			
			wp_reset_postdata();
		}
		
		return $models;
	}
}

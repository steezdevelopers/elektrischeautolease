<?php

/**
 * Class QueryHelper
 */
class QueryHelper
{
    public static function get_result(QueryArguments $arguments)
    {
        $args = [
            'post_type'         => $arguments->get_post_types(),
            'post_status'       => $arguments->get_post_statuses(),
            'paged'             => $arguments->get_paged(),
            'posts_per_page'    => $arguments->get_posts_per_page(),
            'orderby'           => $arguments->get_order_by(),
            'order'             => $arguments->get_order(),
            'meta_key'          => $arguments->get_meta_key(),
            'meta_value'        => $arguments->get_meta_value()
        ];

        $tax_queries = $arguments->get_tax_queries();
        if (!empty($tax_queries)) {
            $args['tax_query'] = self::build_tax_query($tax_queries);
        }

        $excluded_posts = $arguments->get_excluded_posts();
        if (!empty($excluded_posts)) {
            $args['post__not_in'] = $excluded_posts;
        }

        if ($args['orderby'] == 'meta_value' || $args['orderby'] == 'meta_value_num') {
            $args['meta_key'] = $arguments->get_meta_key();
        }

        $args['meta_query'] = $arguments->get_meta_query();

        $result = new Timber\PostQuery($args);

        return new QueryResult($result);
    }

    /**
     * @param array $tax_queries
     * @return array
     */
    private static function build_tax_query(array $tax_queries)
    {
        if (empty($tax_queries)) {
            return [];
        }

        $args = [];
        $or_tax_query = ['relation' => 'OR'];

        foreach ($tax_queries as $key => $queries) {
            /** @var QueryTaxonomy $query */
            foreach ($queries as $query) {
                $filter = [
                    'taxonomy'          => $query->get_taxonomy(),
                    'field'             => $query->get_field(),
                    'terms'             => $query->get_term_ids(),
                    'operator'          => $query->get_operator(),
                    'include_children'  => $query->get_incl_children()
                ];

                if ($key === 'AND') {
                    $args[] = $filter;
                }
                else {
                    $or_tax_query[] = $filter;
                }
            }
        }

        $args[] = $or_tax_query;

        return $args;
    }
}

/**
 * Class QueryResult
 */
class QueryResult
{
    private $result;

    public function __construct(Timber\PostQuery $result)
    {
        $this->result = $result;
    }

    public function get_posts()
    {
        return $this->result->get_posts();
    }

    public function get_pagination($prefs = [])
    {
        return $this->result->pagination($prefs);
    }

    public function get_count()
    {
        return $this->result->found_posts;
    }
}

/**
 * Helper class, to be used with QueryHelper
 */
class QueryArguments
{
    private static $POST_STATUS = [
        'publish',    // - a published post or page
        'pending',    // - post is pending review
        'draft',      // - a post in draft status
        'auto-draft', // - a newly created post, with no content
        'future',     // - a post to publish in the future
        'private',    // - not visible to users who are not logged in
        'trash',      // - post is in trashbin
        'any'
    ];

    private static $ORDER_BY = [
        'none',          // - No order
        'ID',            // - Order by post id (Note the capitalization)
        'title',         // - Order by title
        'name',          // - Order by post name
        'type',          // - Order by post type
        'date',          // - Order by date
        'modified',      // - Order by last modified date
        'menu_order',    // - Order by Page Order. Used most often for Pages
        'meta_value',    // - Order by meta value (note that a 'meta_key=keyname' must also be present in the query)
        'meta_value_num' // - Order by numeric meta value (note that a 'meta_key=keyname' must also be present in the query)
    ];

    private $post_types     = [];
    private $post_statuses  = ['publish'];
    private $excluded_posts = [];
    private $paged          = 1;
    private $posts_per_page = 30;
    private $order          = 'DESC';
    private $order_by       = 'date';
    private $meta_key       = false;
    private $tax_queries    = [];
    private $meta_query     = [];

    public function __construct($post_types = 'any', $posts_per_page = 30, $paged = 1)
    {
        if (!is_array($post_types)) {
            $post_types = [$post_types];
        }

        $this
            ->set_post_types($post_types)
            ->set_posts_per_page($posts_per_page)
            ->set_paged($paged)
        ;
    }

    /**
     * @return array
     */
    public function get_post_types()
    {
        return $this->post_types;
    }

    /**
     * @param array $post_types
     * @return self
     */
    public function set_post_types(array $post_types)
    {
        if (empty($post_types)) {
            throw new InvalidArgumentException('Post types cannot be empty');
        }

        $this->post_types = $post_types;

        return $this;
    }

    /**
     * @return array
     */
    public function get_meta_query()
    {
        return ['relation' => 'AND', $this->meta_query ];
    }

    /**
     * Set the meta query by parsing in a meta query array.
     *
     * @param array $meta_query
     * @return $this
     */
    public function set_meta_query(array $meta_query)
    {
        $this->meta_query[] = $meta_query;
        return $this;
    }

    /**
     * @return array
     */
    public function get_post_statuses()
    {
        return $this->post_statuses;
    }

    /**
     * @param array $post_statuses
     * @return self
     */
    public function set_post_status(array $post_statuses)
    {
        if (empty($post_statuses)) {
            throw new InvalidArgumentException('Post statuses cannot be empty');
        }

        foreach ($post_statuses as $post_status) {
            if (!in_array($post_status, static::$POST_STATUS)) {
                throw new InvalidArgumentException('Invalid post status given - Valid: ' . implode(static::$POST_STATUS, ', '));
            }
        }

        $this->post_statuses = $post_statuses;

        return $this;
    }

    /**
     * @return array
     */
    public function get_excluded_posts()
    {
        return $this->excluded_posts;
    }

    /**
     * @param array|int $excluded_posts
     * @return self
     */
    public function add_exclude_posts($excluded_posts)
    {
        if (is_int($excluded_posts)) {
            $this->excluded_posts[] = $excluded_posts;
        }
        else {
            if (is_array($excluded_posts) && array_filter($excluded_posts, 'is_int')) {
                $this->excluded_posts = array_merge($excluded_posts, $this->excluded_posts);
            }
            else {
                throw new InvalidArgumentException('Invalid excluded post given - must be an int');
            }
        }

        return $this;
    }

    /**
     * @param array|int $excluded_posts
     * @return self
     */
    public function set_exclude_posts($excluded_posts)
    {
        if (is_int($excluded_posts)) {
            $this->excluded_posts = [$excluded_posts];
        }
        else {
            if (is_array($excluded_posts) && array_filter($excluded_posts, 'is_int')) {
                $this->excluded_posts = $excluded_posts;
            }
            else {
                throw new InvalidArgumentException('Invalid excluded post given - must be an int');
            }
        }

        return $this;
    }

    /**
     * @return int
     */
    public function get_paged()
    {
        return $this->paged;
    }

    /**
     * @param int $paged
     * @return self
     */
    public function set_paged($paged)
    {
        if (!is_int($paged)) {
            throw new InvalidArgumentException('Invalid paged given - must be an int');
        }

        $this->paged = $paged;

        return $this;
    }

    /**
     * @return int
     */
    public function get_posts_per_page()
    {
        return $this->posts_per_page;
    }

    /**
     * @param int $posts_per_page
     * @return self
     */
    public function set_posts_per_page($posts_per_page)
    {
        if (!is_int($posts_per_page)) {
            throw new InvalidArgumentException('Invalid posts_per_page given - must be an int');
        }

        $this->posts_per_page = $posts_per_page;

        return $this;
    }

    /**
     * @return string
     */
    public function get_order()
    {
        return $this->order;
    }

    /**
     * @param string $order
     * @return self
     */
    public function set_order($order)
    {
        if (!is_string($order) || empty($order) || 'DESC' === strtoupper($order) ) {
            $this->order = 'DESC';
        }
        else {
            $this->order = 'ASC';
        }

        return $this;
    }

    /**
     * @return string
     */
    public function get_order_by()
    {
        return $this->order_by;
    }

    /**
     * @param string $order_by
     * @return self
     */
    public function set_order_by($order_by)
    {
        if (!in_array($order_by, static::$ORDER_BY)) {
            throw new InvalidArgumentException('Invalid orderby given - Valid: ' . implode(static::$ORDER_BY, ', '));
        }

        $this->order_by = $order_by;

        return $this;
    }

    /**
     * @return bool
     */
    public function get_meta_key()
    {
        return $this->meta_key;
    }

    /**
     * @param bool $meta_key
     */
    public function set_meta_key($meta_key)
    {
        $this->meta_key = $meta_key;
    }

    /**
     * @return bool
     */
    public function get_meta_value()
    {
        return $this->meta_value;
    }

    /**
     * @param bool $meta_value
     */
    public function set_meta_value($meta_value)
    {
        $this->meta_value = $meta_value;
    }

    /**
     * @return array
     */
    public function get_tax_queries()
    {
        return $this->tax_queries;
    }

    /**
     * @return array
     */
    public function set_tax_queries($tax_query)
    {
        $this->tax_query = $tax_query;
    }

    /**
     * @param QueryTaxonomyFilter $filter
     * @return self
     */

    public function add_and_tax_query(QueryTaxonomyFilter $filter)
    {
        if (!isset($this->tax_queries['AND'])) {
            $this->tax_queries['AND'] = [];
        }

        $this->tax_queries['AND'][] = $filter;

        return $this;
    }

    /**
     * @param QueryTaxonomyFilter $filter
     * @return self
     */
    public function add_or_tax_query(QueryTaxonomyFilter $filter)
    {
        if (!isset($this->tax_queries['OR'])) {
            $this->tax_queries['OR'] = [];
        }

        $this->tax_queries['OR'][] = $filter;

        return $this;
    }
}

/**
 * Helper class for querying filtering with taxonomies, to be used with QueryHelper and QueryArguments
 */
class QueryTaxonomyFilter
{
    private static $OPERATORS   = ['IN', 'NOT IN', 'AND', 'EXISTS', 'NOT EXISTS'];
    private static $FIELDS      = ['term_id', 'name', 'slug', 'term_taxonomy_id'];

    public $taxonomy;
    public $term_ids;
    public $operator;
    public $incl_children;
    public $field;

    public function __construct($taxonomy, $term_ids = [], $operator = 'IN', $incl_children = true, $field = 'term_id')
    {
        $this
            ->set_taxonomy($taxonomy)
            ->set_term_ids($term_ids)
            ->set_operator($operator)
            ->set_incl_children($incl_children)
            ->set_field($field)
        ;
    }

    /**
     * @return string
     */
    public function get_taxonomy()
    {
        return $this->taxonomy;
    }

    /**
     * @param string $taxonomy
     * @return self
     */
    public function set_taxonomy($taxonomy)
    {
        if (empty($taxonomy)) {
            throw new InvalidArgumentException('Taxonomy cannot be empty');
        }

        $this->taxonomy = $taxonomy;

        return $this;
    }

    /**
     * @return array
     */
    public function get_term_ids()
    {
        return $this->term_ids;
    }

    /**
     * @param array $term_ids
     * @return self
     */
    public function set_term_ids(array $term_ids)
    {
        if (empty($term_ids)) {
            throw new InvalidArgumentException('Term ids cannot be empty');
        }

        $this->term_ids = $term_ids;

        return $this;
    }

    /**
     * @return string
     */
    public function get_operator()
    {
        return $this->operator;
    }

    /**
     * @param string $operator
     * @return self
     */
    public function set_operator($operator)
    {
        if (!in_array($operator, static::$OPERATORS)) {
            throw new InvalidArgumentException('Invalid operator given - Valid: ' . implode(static::$OPERATORS, ', '));
        }

        $this->operator = $operator;

        return $this;
    }

    /**
     * @return bool
     */
    public function get_incl_children()
    {
        return $this->incl_children;
    }

    /**
     * @param bool $incl_children
     * @return self
     */
    public function set_incl_children($incl_children)
    {
        $this->incl_children = !!($incl_children);

        return $this;
    }

    /**
     * @return string
     */
    public function get_field()
    {
        return $this->field;
    }

    /**
     * @param string $field
     * @return self
     */
    public function set_field($field)
    {
        if (!in_array($field, static::$FIELDS)) {
            throw new InvalidArgumentException('Invalid field given - Valid: ' . implode(static::$FIELDS, ', '));
        }

        $this->field = $field;

        return $this;
    }
}
<?php
/**
 * reviews
 *
 * @copyright Copyright © 2021 Steez Webdevelopment. All rights reserved.
 * @author    tommy@steez.nl
 */

namespace Brandfirm\Devkit;


use WP_Query;

class Reviews
{
	
	/**
	 * Reviews constructor.
	 */
	public function __construct ()
	{
		add_filter('timber/context', [$this, 'init_reviews'], 10, 1);
		add_filter('timber/context', [$this, 'init_slider_reviews'], 10, 1);
		
	}
	
	/**
	 * Add the deals table to the single model page.
	 * @param $context
	 * @return mixed
	 */
	public function init_reviews ($context)
	{
		$context['review_settings'] = get_fields('review_settings');
		
		$reviews = [];
		
		if (is_post_type_archive('models')) {
			$reviews = self::get_reviews();
			
			if (!empty($reviews)) {
				$context['reviews'] = $reviews;
			}
			
			wp_reset_postdata();
		}
		
		if (is_singular('models')) {
			$reviews = self::get_reviews(4);
			
			if (!empty($reviews)) {
				$context['reviews'] = $reviews;
			}
			
			wp_reset_postdata();
		}
		
		return $context;
	}
	
	/**
	 * @param int $posts_per_page
	 * @return array
	 */
	public static function get_reviews ($posts_per_page = -1): array
	{
		$reviews = [];
		
		$args = [
			'post_type' => 'review',
			'post_status' => 'publish',
			'pagination' => true,
			'posts_per_page' => -1,
		];
		
		$query = new WP_Query($args);
		
		if ($query->have_posts()) {
			while ($query->have_posts()) {
				$query->the_post();
				global $post;
				$review = get_post(get_the_ID());
				if (!empty($review)) {
					$reviews[] = self::get_review($review);
				}
			}
		}
		
		return $reviews;
	}
	
	/**
	 * @param $review_post
	 * @return array
	 */
	public static function get_review ($review_post): array
	{
		$review_item = [];
		
		$image_src = '';
		$image_id = get_field('review_image', $review_post->ID);
		if (!empty($image_id)) {
			$image_src = wp_get_attachment_image_src($image_id, 'review_item');
			if (!empty($image_src)) {
				$image_src = $image_src[0];
			}
		}
		
		// Add all necessary custom fields to the reviews array.
		$review_item['review_image'] = $image_src;
		$review_item['review_name'] = get_the_title($review_post->ID);
		$review_item['review_subtitle'] = get_field('review_subtitle', $review_post->ID);
		$review_item['review_text'] = get_field('review_text', $review_post->ID);
		$review_item['review_link'] = get_field('review_archive_link', 'review_settings');
		
		/** @todo Excerpt, nog kleiner maken */
		$review_item['review_excerpt'] = wp_trim_excerpt(get_field('review_text', $review_post->ID));
		$review_item['review_car'] = [
			'link' => get_permalink(get_field('review_car', $review_post->ID)),
			'title' => get_the_title(get_field('review_car', $review_post->ID))
		];
		
		return $review_item;
	}
	
	/**
	 * @param $context
	 * @return mixed
	 */
	public function init_slider_reviews ($context)
	{
		$sections = 'bf_sections';
		$reviews = [];
		
		if ($sections = get_field('bf_sections')) {
			// Sections exist, is the slider in there?
			
			if (have_rows('bf_sections')) {
				while (have_rows('bf_sections')) {
					the_row();
					if (get_row_layout() == 'reviews') {
						if ($review_item_ids = get_sub_field('reviews')) {
							/** @var $review_item_id int */
							foreach ($review_item_ids as $review_item_id) {
								$reviews[] = self::get_review(get_post($review_item_id));
							}
						}
					}
				}
			}
		}
		
		if (!empty($reviews)) {
			$context['reviews_slider'] = $reviews;
		}
		
		return $context;
	}
}

new Reviews();

<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the General Public License (GPL 3.0).
 * This license is available through the world-wide-web at this URL:
 * http://opensource.org/licenses/gpl-3.0.en.php
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this module to newer
 * versions in the future.
 *
 * @author      Tommy Kolkman <tommy@steez.nl>
 * @copyright   Copyright (c) 2020 Steez (https://steez.nl)
 * @license     http://opensource.org/licenses/gpl-3.0.en.php General Public License (GPL 3.0)
 *
 */

/**
 * The return function for the shortcode [car_details_in_quote_form]
 */
function car_details_in_quote_form ()
{
	$return_value = '';
	$model_id = ill_car_details_get_model_id();
	$deal_id = !empty(ill_car_details_get_deal_id()) ? ill_car_details_get_deal_id() : 0;
	
	if (!empty($model_id)) {
		$car_model = \Timber\Timber::get_post($model_id);
		
		$specifications = [
			[
				'label' => 'Referentienummer',
				'value' => $car_model->get_field('reference')
			],
			[
				'label' => 'Model',
				'value' => $car_model->get_field('model_name')
			],
			[
				'label' => 'Versie',
				'value' => $car_model->get_field('model_version')
			],
			[
				'label' => 'Actieradius',
				'value' => $car_model->get_field('range_real') . 'km'
			],
		];
		
		$type = !empty($_GET['type']) ? esc_html($_GET['type']) : 'business';
		
		$show_color_choice_label = 1;
		if (has_term(ModelSingle::STOCK_CAR_TERM, 'lease', $car_model->ID)) {
			$specifications = [];
			
			$show_color_choice_label = 0;
			
			$usps[] = $car_model->get_field('usp_car_text1');
			$usps[] = $car_model->get_field('usp_car_text2');
			$usps[] = $car_model->get_field('usp_car_text3');
			$usps[] = $car_model->get_field('usp_car_text4');
			
			
			foreach ($usps as $usp) {
				if (!empty($usp)) {
					$usp_splitted = explode(':', $usp);
					if (!empty($usp_splitted[0]) && !empty($usp_splitted[1])) {
						$specifications[] = [
							'label' => trim($usp_splitted[0], " "),
							'value' => trim($usp_splitted[1], " ")
						];
					} else {
						$specifications[] = [
							'label' => " ",
							'value' => $usp
						];
					}
				}
			}
		}
		
		$context = Timber\Timber::context();
		$context['car_model'] = [
			'post' => $car_model,
			'name' => $car_model->get_field('custom_seo_title'),
			'specifications' => $specifications,
			'type' => $type,
			'show_color_choice_label' => $show_color_choice_label
		];
		
		ob_start();
		\Timber\Timber::render(array('components/car-details-in-quote-form.twig'), $context);
		$return_value = ob_get_contents();
		ob_end_clean();
	}
	
	return $return_value;
}

/**
 * Register the shortcode [car_details_in_quote_form]
 */
add_shortcode('car_details_in_quote_form', 'car_details_in_quote_form');

/**
 * Get the model ID from the string.
 * @return string
 */
function ill_car_details_get_model_id ()
{
	$model_id = '';
	if (!empty($_GET['model_id'])) {
		$model_id = esc_html($_GET['model_id']);
	}
	
	return $model_id;
}

/**
 * Get the model ID from the string.
 * @return string
 */
function ill_car_details_get_deal_id ()
{
	$deal_id = '';
	if (!empty($_GET['deal_id'])) {
		$deal_id = esc_html($_GET['deal_id']);
	}
	
	return $deal_id;
}

/**
 * CTA shortcode fallback
 * @param $atts
 * @return string
 */
function cta_shortcode_callback ($atts)
{
	$return_value = '';
	
	if (!empty($atts['id']) && is_numeric($atts['id'])) {
		$shortcode_data = [];
		
		/**
		 * Get the basic CTA data.
		 */
		if ($cta_intro = get_field('cta_intro', $atts['id'])) {
			$shortcode_data['intro'] = $cta_intro;
		}
		if ($cta_button = get_field('cta_button', $atts['id'])) {
			$shortcode_data['button'] = $cta_button;
		}
		
		/**
		 * Check if there's USP data in this CTA.
		 */
		$shortcode_data['usps'] = [];
		if (!empty(get_field('cta_usps', $atts['id'])) && !empty($atts['usps']) && '1' === $atts['usps']) {
			$usps = get_field('cta_usps', $atts['id']);
			if (!empty($usps)) {
				foreach ($usps as $usp) {
					$shortcode_data['usps'][] = $usp['usp_text'];
				}
			}
		}
		
		if (!empty($shortcode_data)) {
			$context = Timber\Timber::context();
			$context['cta_shortcode'] = $shortcode_data;
			
			ob_start();
			\Timber\Timber::render(array('components/cta-shortcode.twig'), $context);
			$return_value = ob_get_contents();
			ob_end_clean();
		}
	}
	
	return $return_value;
}

add_shortcode('cta', 'cta_shortcode_callback');

/**
 * @param $atts
 * @param $content
 * @return false|string
 */
function shortcode_citation ($atts, $content)
{
	$return_value = '';
	
	if (!empty($content)) {
		
		$context['shortcode_citation']['content'] = $content;
		$context['shortcode_citation']['person'] = false;
		
		if (!empty($atts['person'])) {
			$context['shortcode_citation']['person'] = $atts['person'];
		}
		
		ob_start();
		\Timber\Timber::render(array('components/shortcode-citation.twig'), $context);
		$return_value = ob_get_contents();
		ob_end_clean();
	}
	
	return $return_value;
}

add_shortcode('citation', 'shortcode_citation');

/**
 * @param $atts
 * @return string
 */
function action_button_shortcode ($atts, $content = "")
{
	return sprintf(
		'<a href="%s" target="%s" class="button button__primary button__arrow"><span>%s</span><svg class="icon button__icon" width="7" height="12" xmlns="http://www.w3.org/2000/svg"><path d="M4.586 6L.293 1.707A1 1 0 1 1 1.707.293l5 5a1 1 0 0 1 0 1.414l-5 5a1 1 0 1 1-1.414-1.414L4.586 6z" fill="#051033" fill-rule="nonzero"></path></svg></a>',
		!empty($atts['link']) ? $atts['link'] : '#0',
		!empty($atts['target']) ? $atts['target'] : '_self',
		!empty($content) ? $content : 'Text'
	);
}

/* Shortcode to display an action button. */
add_shortcode('button', 'action_button_shortcode');

<?php
// Add custom column to the admin screen
function custom_admin_column_head($defaults) {
    $defaults['price_column'] = 'Price';
    return $defaults;
}

add_filter('manage_edit-models_columns', 'custom_admin_column_head');

// Display the custom column value
function custom_admin_column_content($column_name, $post_ID) {
    if ($column_name === 'price_column') {
        // Replace 'price' with the actual ACF field name
        $price_value = get_field('price', $post_ID);

        // Output the ACF value in the custom column
        echo esc_html($price_value);
    }
}

add_action('manage_models_posts_custom_column', 'custom_admin_column_content', 10, 2);

// Make the custom column sortable
function custom_admin_column_sortable($columns) {
    $columns['price_column'] = 'price';
    return $columns;
}

add_filter('manage_edit-models_sortable_columns', 'custom_admin_column_sortable');

// Modify the query to sort by the custom column
function custom_admin_column_orderby($query) {
    if (!is_admin() || !$query->is_main_query()) {
        return;
    }

    $orderby = $query->get('orderby');

    if ('price' === $orderby) {
        $query->set('meta_key', 'price');
        $query->set('orderby', 'meta_value_num');
    }
}

add_action('pre_get_posts', 'custom_admin_column_orderby');

<?php

/** Taxonomies @package Brandfirm */
class BrandfirmTaxonomies
{
	
	const TAG = 'post_tag';   // Default tag taxonomy
	const CATEGORY = 'category';   // Default category taxonomy
	const BRAND = 'brand';      // Default model taxonomy
	const TYPE = 'type';
	const SEATS = 'seats';
	const LEASE = 'lease';
	const MODELLANDINGSPAGE = 'modellandingspage';
	const AVAILABILITY = 'availability';
	const TAX_ADDITION = 'tax_addition';
	const DELIVERY_TIMES = 'delivery_time';
	const LEASE_COMPANY = 'lease_company';
	
	function register_all ()
	{
		$this->register_brand();
		$this->register_type();
		$this->register_seats();
		$this->register_lease();
		$this->register_modellandingspage();
		$this->register_availability();
		$this->register_tax_addition();
		
		// Temporarily off
		 $this->register_delivery_times();
		
		// Temp taxonomy:
		$this->register_lease_companies();
	}
	
	function get_default_args ($taxonomy, $name, $singular_name, $menu_name, $slug)
	{
		$labels = [
			'name' => _x($name, 'taxonomy general name'),
			'singular_name' => _x($singular_name, 'taxonomy singular name'),
			'menu_name' => _x($menu_name, 'taxonomy menu name'),
			'all_items' => sprintf(__('All %s'), __($name)),
			'edit_item' => sprintf(__('Edit %s'), $singular_name),
			'view_item' => sprintf(__('View %s'), $singular_name),
			'update_item' => sprintf(__('Update %s'), $singular_name),
			'add_new_item' => __('Add new'),
			'new_item_name' => sprintf(__('New %s Name'), $singular_name),
			'parent_item' => sprintf(__('Parent %s'), $singular_name),
			'parent_item_colon' => sprintf(__('Parent %s:'), $singular_name),
			'search_items' => sprintf(__('Search %s'), $name),
			'popular_items' => sprintf(__('Popular %s'), $name),
			'separate_items_with_commas' => sprintf(__('Separate %s with commas'), strtolower($name)),
			'add_or_remove_items' => sprintf(__('Add or remove %s'), strtolower($name)),
			'choose_from_most_used' => __('Choose from the most used'),
			'not_found' => __('Not found'),
		];
		
		return [
			'labels' => $labels,
			'public' => true,
			'show_ui' => true,
			'show_in_menu' => true,
			'show_in_nav_menus' => true,
			'show_tagcloud' => false,
			'show_in_quick_edit' => true,
			'show_admin_column' => false,
			'hierarchical' => true,
			'query_var' => $taxonomy,
			'rewrite' => ['slug' => $slug]
		];
	}
	
	function register_lease_companies ()
	{
		$taxonomy = self::LEASE_COMPANY;
		$args = $this->get_default_args(
			$taxonomy,
			'Leasemaatschappijen',
			'Leasemaatschappij',
			'Leasemaatschappijen',
			'leasecompany'
		);
		
		register_taxonomy($taxonomy, [], $args);
	}
	
	function register_brand ()
	{
		$taxonomy = self::BRAND;
		$args = $this->get_default_args(
			$taxonomy,
			'Merk',
			'Merk',
			'Merken',
			'merken'
		);
		
		register_taxonomy($taxonomy, [], $args);
	}
	
	function register_type ()
	{
		$taxonomy = self::TYPE;
		$args = $this->get_default_args(
			$taxonomy,
			'Type',
			'Type',
			'Types',
			'type'
		);
		
		register_taxonomy($taxonomy, [], $args);
	}
	
	function register_seats ()
	{
		$taxonomy = self::SEATS;
		$args = $this->get_default_args(
			$taxonomy,
			'Seat',
			'Seat',
			'Seats',
			'seats'
		);
		
		register_taxonomy($taxonomy, [], $args);
	}
	
	function register_lease ()
	{
		$taxonomy = self::LEASE;
		$args = $this->get_default_args(
			$taxonomy,
			'Lease',
			'Lease',
			'Leases',
			'lease'
		);
		
		register_taxonomy($taxonomy, [], $args);
	}
	
	function register_modellandingspage ()
	{
		$taxonomy = self::MODELLANDINGSPAGE;
		$args = $this->get_default_args(
			$taxonomy,
			'Landingspagina&lsquo;s voor modellen',
			'Modellandingspagina',
			'Landingspagina&lsquo;s voor modellen',
			'modellandingspagina'
		);
		
		register_taxonomy($taxonomy, [], $args);
	}
	
	// Helper functions
	
	function register_availability ()
	{
		$taxonomy = self::AVAILABILITY;
		$args = $this->get_default_args(
			$taxonomy,
			'Beschikbaarheid',
			'Status',
			'Beschikbaarheid',
			'availability'
		);
		register_taxonomy($taxonomy, [], $args);
	}
	
	function register_tax_addition ()
	{
		$taxonomy = self::TAX_ADDITION;
		$args = $this->get_default_args(
			$taxonomy,
			'Bijtelling',
			'Bijtelling',
			'Bijtelling',
			'tax_addition'
		);
		register_taxonomy($taxonomy, [], $args);
	}
	
	function register_delivery_times ()
	{
		$taxonomy = self::DELIVERY_TIMES;
		$args = $this->get_default_args(
			$taxonomy,
			'Levertijd',
			'Levertijd',
			'Levertijd',
			'delivery_time'
		);
		register_taxonomy($taxonomy, [], $args);
	}
}

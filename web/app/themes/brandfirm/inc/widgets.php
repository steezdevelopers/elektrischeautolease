<?php

// Sidebar
add_action('widgets_init', 'brandfirm__widgets_init');

function brandfirm__widgets_init() {
	register_sidebar([
		'name' => 'Footer column 1',
		'id' => 'footer-column1',
		'before_widget'  => '<div class="footer--menu">',
		'after_widget' => '</div>',
		'before_title' => '<h3>',
		'after_title' => '</h3>'
	]);

	register_sidebar([
		'name' => 'Footer column 2',
		'id' => 'footer-column2',
		'before_widget'  => '<div class="footer--menu">',
		'after_widget' => '</div>',
		'before_title' => '<h3>',
		'after_title' => '</h3>'
	]);

	register_sidebar([
		'name' => 'Footer column 3',
		'id' => 'footer-column3',
		'before_widget'  => '<div class="footer--menu">',
		'after_widget' => '</div>',
		'before_title' => '<h3>',
		'after_title' => '</h3>'
	]);

	register_sidebar([
		'name' => 'Footer column 4',
		'id' => 'footer-column4',
		'before_widget'  => '<div class="footer--menu">',
		'after_widget' => '</div>',
		'before_title' => '<h3>',
		'after_title' => '</h3>'
	]);

	register_sidebar([
		'name' => 'Sidebar Landingspage',
		'id' => 'sidebar',
		'before_widget'  => '<div class="sidebar__block">',
		'after_widget' => '</div>',
		'before_title' => '<h3>',
		'after_title' => '</h3>'
	]);

		
		
}
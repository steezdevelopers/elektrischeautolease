<?php
/**
 * The main template file
 *
 * @package brandfirm-setup
 */

$context = Timber\Timber::get_context();
$post = Timber\Timber::get_post();
$context['post'] = $post;

/**
 * Blog integration
 */
$context = set_blogs_to_context();

Timber\Timber::render([
	'archive-' . get_post_type() . '.twig',
	'index.twig'
], $context);

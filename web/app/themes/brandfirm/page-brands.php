<?php
/**
 * Template Name: Brands
 */
$post = new LandingPage();
$context = Timber\Timber::context();
$context['post'] = $post;
$context['model_collection_list'] = SectionHelper::get_model_collection_list();

Timber::render( array(
    'page-' . $post->post_name . '.twig',
    'page.twig'
), $context );

?>

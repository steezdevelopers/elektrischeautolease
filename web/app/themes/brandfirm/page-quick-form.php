<?php
/**
 * Template Name: Quick Form Private Lease
 */
/** Template for displaying all default pages @package brandfirm */
$post = new LandingPage();
$context = Timber\Timber::context();
$context['post'] = $post;

$config_data = '{"mediaId":{"daisycon":338736,"xpartners":null},"language":"nl","subId":{"daisycon":"pl-vergelijker","xpartners":null},"locale":"nl-NL","showFilters":true,"limit":50,"colorPrimary":"#08212f","colorSecondary":"#00bf86","filter":{"bodyType":{"value":null},"brand":{"value":null},"condition":{"value":null},"duration":{"min":12,"max":60},"fuelType":{"value":"electric"},"gearSystem":{"value":null},"kmPerYear":{"min":5000,"max":100000}},"buttonText":"Bekijk auto"}';
if (!empty($_GET['quick_form_brand']) ||
	!empty($_GET['quick_form_type']) ||
	!empty($_GET['quick_form_km']) ||
	!empty($_GET['quick_form_contract'])) {
	
	$quick_form = [];
	
	$config_data_decoded = json_decode($config_data, true);
	
	if (!empty($_GET['quick_form_brand'])) {
		$config_data_decoded['filter']['brand']['value'] = [esc_html($_GET['quick_form_brand'])];
	}
	if (!empty($_GET['quick_form_type'])) {
		$config_data_decoded['filter']['bodyType']['value'] = [esc_html($_GET['quick_form_type'])];
	}
	if (!empty($_GET['quick_form_km'])) {
		$config_data_decoded['filter']['kmPerYear']['max'] = (int)esc_html($_GET['quick_form_km']);
	}
	if (!empty($_GET['quick_form_contract'])) {
		$config_data_decoded['filter']['duration']['max'] = (int)esc_html($_GET['quick_form_contract']);
	}
	
	$context['quick_form_private_lease_config'] = json_encode($config_data_decoded);
	
} else {
	$context['quick_form_private_lease_config'] = $config_data;
}


Timber::render(array(
	'page-quick-form.twig',
	'page.twig'
), $context);

?>

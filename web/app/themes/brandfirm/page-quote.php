<?php
/**
 * Template Name: Quote Form
 */
$post = new LandingPage();
$context = Timber\Timber::context();
$context['post'] = $post;
$context['brands'] = Timber::get_terms('brand');
$context['sections'] = SectionHelper::decorate(get_field('bf_sections'));

Timber::render(array(
	'page-quote.twig',
	'page.twig'
), $context);

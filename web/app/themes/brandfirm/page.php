<?php
/** Template for displaying all default pages @package brandfirm */
$post = new LandingPage();
$context = Timber\Timber::context();
$context['post'] = $post;
$context['brands'] = Timber::get_terms('brand');
$context['car_types'] = Timber::get_terms('type');

$context['car_svgs'] = [
    'bestelwagen' => ill_get_template_part('views/svg/cars/bestelwagen', [], false),
    'cabrio' => ill_get_template_part('views/svg/cars/cabrio', [], false),
    'hatchback' => ill_get_template_part('views/svg/cars/hatchback', [], false),
    'liftback_sedan' => ill_get_template_part('views/svg/cars/liftback_sedan', [], false),
    'mpv' => ill_get_template_part('views/svg/cars/mpv', [], false),
    'sedan' => ill_get_template_part('views/svg/cars/sedan', [], false),
    'suv' => ill_get_template_part('views/svg/cars/suv', [], false),
    'default' => ill_get_template_part('views/svg/cars/default', [], false),
];

Timber::render( array(
    'page-' . $post->post_name . '.twig',
    'page.twig'
), $context );

?>

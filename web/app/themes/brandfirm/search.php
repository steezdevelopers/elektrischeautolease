<?php
global $wp_query;
$query = new \Timber\PostQuery($wp_query);
$context = \Timber\Timber::get_context();

$context['posts']       = $query->get_posts();
$context['pagination']  = $query->pagination();
$context['total']       = $wp_query->found_posts;
$context['post_types']  = 'models';
$context['search_term'] = get_search_query();

\Timber\Timber::render(array('search.twig'), $context);
?>
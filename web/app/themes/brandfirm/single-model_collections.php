<?php
/**
 * Template for displaying all single posts
 *
 * @package brandfirm-setup
 */
$context = Timber\Timber::get_context();
$post = Timber\Timber::get_post();
$context['post'] = $post;

$context['landingpage'] = false;

if ( $model_collection_link_taxonomy = get_field( 'model_collection_link_taxonomy' ) ){
    try {
        $context['landingpage'] = get_term_link($model_collection_link_taxonomy, BrandfirmTaxonomies::MODELLANDINGSPAGE);
    } catch (Exception $e) {
    }
}

Timber\Timber::render( array(
    'single-model_collections.twig',
    'page.twig'
), $context );

?>

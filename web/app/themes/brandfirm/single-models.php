<?php
/**
 * Template for displaying all single posts
 *
 * @package brandfirm-setup
 */
$context = Timber\Timber::get_context();
$post = Timber\Timber::get_post();
$context['post'] = $post;

$context['post']->quote_enabled = get_field('quote_enabled', 'options');
$context['post']->is_occasion = get_field('configurator_occasion');

$context['post']->is_new_stock = 0;
if (has_term(ModelSingle::STOCK_CAR_TERM, 'lease', get_the_ID())) {
	$context['post']->is_new_stock = 1;
}

Timber\Timber::render(array(
	'single-models' . '.twig',
), $context);

?>

<?php
/**
 * Template for displaying all single posts
 *
 * @package brandfirm-setup
 */
$context = Timber\Timber::get_context();
$post = Timber\Timber::get_post();
$context['post'] = $post;

/**
 * Blog integration
 */
if (get_post_type() === 'post') {
	if ($acf_related_post_ids = get_field('related_posts')) {
		$related_posts = Timber\Timber::get_posts([
			'post__in' => $acf_related_post_ids
		]);
		if (!empty($related_posts)) {
			foreach ($related_posts as $related_post) {
				$context['related_posts'][] = [
					'model' => $related_post,
					'excerpt' => get_the_excerpt($related_post->ID)
				];
			}
		}
		
	}
}

Timber\Timber::render(array(
	'single-' . $post->post_name . '.twig',
	'single-' . $post->post_type . '.twig',
	'single.twig'
), $context);

?>

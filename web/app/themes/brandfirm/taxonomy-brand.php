<?php

/**
 * Template for displaying all single posts
 *
 * @package brandfirm-setup
 */
$context = Timber\Timber::get_context();
$post = Timber\Timber::get_post();
$context['post'] = $post;

$context['posts'] = new Timber\PostQuery();
$context['pagination'] = Timber::get_pagination();
$context['type'] = Timber::get_terms('type');
$context['leases'] = Timber::get_terms([
	'taxonomy' => 'lease',
	'hide_empty' => true,
	'meta_query' => [
		[
			'key' => 'lease_show_in_filter',
			'value' => 1,
		],
	]
]);
$context['seats'] = Timber::get_terms('seats');

$queried_object = get_queried_object();
$term_id = $queried_object->term_id;
$context['term'] = new Timber\Term($term_id);

$context['count'] = count($posts);

Timber\Timber::render(array(
	'taxonomy-brand.twig',
	'page.twig'
), $context);

?>

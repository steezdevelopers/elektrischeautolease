<?php

/**
 * Template for displaying all single posts
 *
 * @package brandfirm-setup
 */
$context = Timber\Timber::get_context();
$post = Timber\Timber::get_post();
$context['post'] = $post;

$context['posts'] = new Timber\PostQuery();
$context['pagination'] = Timber::get_pagination();
$context['type'] = Timber::get_terms('type');
$context['brands'] = Timber::get_terms('brand');
$context['seats'] = Timber::get_terms('seats');

$queried_object = get_queried_object();
$term_id = $queried_object->term_id;
$context['term'] = new Timber\Term($term_id);
$context['lease_id'] = $term_id;

Timber\Timber::render( array(
    'taxonomy-lease.twig',
    'page.twig'
), $context );

?>

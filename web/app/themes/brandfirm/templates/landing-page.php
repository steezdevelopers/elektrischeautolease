<?php
/** Template Name: Landing Page 
 * Template Post Type: page 
 * @package brandfirm */

$post = new LandingPage(); // Not using Timber::get_post(); so we can use LandingPage instead of PagePost
$context = Timber\Timber::context();
$context['post'] = $post;

Timber\Timber::render('templates/landing-page.twig', $context);
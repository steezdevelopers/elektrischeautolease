<?php
/** Template Name: Review Page
 * Template Post Type: page
 * @package brandfirm
 */

$post = new LandingPage(); // Not using Timber::get_post(); so we can use LandingPage instead of PagePost
$context = Timber\Timber::context();
$context['post'] = $post;
$context['reviews'] = \Brandfirm\Devkit\Reviews::get_reviews();

Timber\Timber::render('templates/review-page.twig', $context);

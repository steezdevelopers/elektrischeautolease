const Encore            = require('@symfony/webpack-encore'),
      BrowserSyncPlugin = require('browser-sync-webpack-plugin');
      dotenv            = require('dotenv');

const env = dotenv.config();

if (env.error) {
    throw env.error;
} else {
    console.log(`Theme: ${process.env.THEME}`)
}

Encore
    .setOutputPath(`dist/`)
    .setPublicPath(`/app/themes/brandfirm/dist`)
    .configureBabel(function(babelConfig) {
        babelConfig.plugins.push("@babel/plugin-proposal-class-properties");
    })
    .addEntry('js/app', './assets/js/app.js')
  .autoProvidejQuery()
  .addLoader({
    test: /\.scss$/,
    enforce: 'pre',
    loader: 'import-glob-loader',
  })
  .enableSassLoader(function (sassOptions) { }, {
    resolveUrlLoader: false
  })
  .enablePostCssLoader((options) => { options.config = { path: 'postcss.config.js' } })
  .enableBuildNotifications()
    .addStyleEntry('css/app', './assets/scss/app.scss')
    .copyFiles([{ from: './assets/images', to: 'images/[path][name].[ext]' }])
  .copyFiles([{ from: './assets/fonts', to: 'fonts/[path][name].[ext]' }])
  // .enablePostCssLoader((options) => { options.config = { path: 'postcss.config.js' } })
    .configureTerserPlugin(options => options.extractComments = true)
    .cleanupOutputBeforeBuild()
    .enableSourceMaps(!Encore.isProduction())
    .enableVersioning(Encore.isProduction())
    .disableSingleRuntimeChunk()
;

const config = Encore.getWebpackConfig();
config.watchOptions = {poll: 1000, ignored: /node_modules/}; // Limit file updates for Docker
if (!config.devServer)
config.plugins.push( // Add Browsersync when no dev server
  new BrowserSyncPlugin(
      {
        host: 'localhost',
        port: '3030',
        proxy: 'https://elektrischeautolease.steezlabs.nl/',
        open: false,
        notify: false,  // Don't show any notifications in the browser.
        files: [ // watch on changes
            {
                match: [`themes/${process.env.THEME}/**`],
                fn: function (event, file) {
                    if (event === 'change') {
                        const bs = require('browser-sync').get('bs-webpack-plugin');
                        bs.reload();
                    }
                }
            }
        ],
      },
      {
          injectCss: true,
          reload: false, // This allows webpack server to take care of instead browser sync
          name: 'bs-webpack-plugin',
      },
  )
);

module.exports = config;
